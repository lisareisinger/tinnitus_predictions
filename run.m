%% TO DO
% check path setting in prepare_cfg
% check ptb_config.real_experiment_sbg_cdk(true); in config_ptb

%% init...
%just to piss off Juliane and Thomas :)
eval("cd('/home/schmidtfa/experiments/SBGHearingMeg')");
exp.init.config_ptb;

warning('Before we start we do shoebox and an audiogram!!!')

%% set variables....
subject_id = '19611009hrkh';

%% prepare...
exp.parts.prepare_subject(subject_id)

%% do resting state 
% set volume to min!
exp.parts.resting(subject_id)
%name resting file as -> subject_id_resting

%% adjust audio individually
%(dont question this again it somehow makes sense)
exp.parts.matchambientlevel(subject_id)

%% do actual Experiment
exp.parts.audio_conti(subject_id)
% name file as subject_id_hdr

%% do markov
% two blocks
exp.parts.markov(subject_id,2)
% name file as subject_id_markov_blkX
 
  

