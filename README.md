# tinnitus_predictions

This repository contains the full MEG study conducted to produce the data stored in https://gin.g-node.org/lisareisinger/tinnitus_predictions (preprocessed form). 
The experiment contained four different pure (sinusoidal) tones, with carrier frequencies were logarithmically spaced between approximately 400 and 1000Hz (i.e. 440 Hz, 587 Hz,782 Hz, 1043 Hz). Overall, 3000 stimuli were presented, half of them in a random order and half of them predictable.
The experiment was written using the MATLAB-based (version 9.1The MathWorks, Natick, Massachusetts, U.S.A) Psychophysics Toolbox (Brainard,1997). We used a whole-head MEG (Triux, MEGINOy, Finland) including 102 magnetometers and 204 orthogonally placed planar gradiometers.

Additional pure-tone audiometry was performed for frequencies from .125to 8 kHz using an Interacoustic AS608 audiometer, which is not included in these files.


The folder "scripts" contains the analysis scripts used to derive the reported results.
