function [bayes_cfg, response] = volume_bayes_present(cfg, sound, bayes_cfg)
%VOLUME_BAYES_PRESENT Summary of this function goes here
%   Detailed explanation goes here
ptb = o_ptb.PTB.get_instance;

sound.db = -bayes_cfg.thresh

stims = [];
stims.question_text = o_ptb.stimuli.visual.Text(cfg.question_text);
stims.yes_text = o_ptb.stimuli.visual.Text(cfg.yes_text);
stims.no_text = o_ptb.stimuli.visual.Text(cfg.no_text);
stims.fixcross = o_ptb.stimuli.visual.FixationCross();

stims.yes_text.sy = 700;
stims.no_text.sy = 770;

ptb.draw(stims.fixcross);
ptb.flip();

this_delay = cfg.stim_delay_avg + randn()*cfg.stim_delay_jitter;

ptb.prepare_audio(sound);
ptb.schedule_audio;

WaitSecs(this_delay);
ptb.play_without_flip;
WaitSecs(sound.duration + cfg.additional_post_stim_delay);

ptb.draw(stims.question_text);
ptb.draw(stims.yes_text);
ptb.draw(stims.no_text);
ptb.flip();

resp = ptb.wait_for_keys({cfg.yes_response, cfg.no_response});

response = ~strcmp(resp{1}, cfg.yes_response);

end

