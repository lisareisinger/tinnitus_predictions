%% VOLUME knob up to the max !!! remember to bring it to the middle afterwards
% Just run this one ... 10000 trials
% remember that Fs in the MEG shuold be 10kHz !!!!
function run_abr_gd(subject_id)

data_path = 'data_pilot';
%% load subject_data
load(fullfile(data_path, subject_id));

%% show warning dialog
exp.parts.helper.show_loudness_warn_dlg('high')

%setup ptb depending on system 
%load 'o_ptb_paths.mat';
%hostname = matlab.lang.makeValidName(char(java.net.InetAddress.getLocalHost().getHostName()));
%hostname = strtok(hostname,'_'); %shorten hostename to make it network independent
%paths = eval(hostname); % ask thomas and gianpaolo for a better solution here
ptb_path = '/home/schmidtfa/experiments/Psychtoolbox-3/';
%ptb_path = '/Users/b1059770/Psychtoolbox/Psychtoolbox-3/';

addpath(ptb_path)
%% check if this has already been run
%if subj_data.abr_done
%  error('ABR already done');
%end
%% ask if ready...
fprintf('All is ready. Press the Space key to begin...\n');
KbWait();

cfg = [];
cfg.volume=1 ;
cfg.nTrials=5000;
cfg.frequency=30;
cfg.meg = 1;         % put 1 if you want to use datapixx                                                            
cfg.testmode = 0;
cfg.stereo = 1;      % 0 or 1, the clinical ecochg does only mono and the other as reference
cfg.alternate= 0;    % 0 or 1, 1 is compression OR rarefaction, otherwise normally only one of the two
cfg.jitter = 1;      % 0 or 1, depends
o_ptb_helper.betterECochGNS(cfg);

%% Goodbye
subj_data.abr_done = true;
save(fullfile(data_path, subject_id), 'subj_data');
%% ctrl-c to stop it, sca to clean PTB
% better stop it with ESC