function betterECochGNS(cfg)
%marginally better wo Screen calls

if isfield(cfg,'volume')
    volume =cfg.volume;
else
    volume =0.5 ;
end

if isfield(cfg,'duration')
    duration =cfg.duration;
else
    duration =0.0001 ; %100 us default duration
end

if isfield(cfg,'stereo')
    stereo =cfg.stereo;
else
    stereo =0; %mono
end

if isfield(cfg,'nTrials')
    nTrials =cfg.nTrials;
else
    nTrials =500; % ???? NW: do we need all the condiitons balanced within a block?
    % overall is yes,
end

% flipped for inside the meg msr
if isfield(cfg,'frequency')
    frequency = cfg.frequency;
else
    frequency = 40; %default repetition thing-y
end


% flipped for inside the meg msr
if isfield(cfg,'Fs')
    Fs = cfg.Fs;
else
    Fs = 96000;
end

% if MEG do clever stuff (Datapixx) otherwise behav with PsychPortAudio
if isfield(cfg,'meg')
    meg = cfg.meg;
else
    meg = 0;
end

if isfield(cfg,'testmode')
    testmode = cfg.testmode;
else
    testmode = 1;
end

if isfield(cfg,'alternate')
    alternate = cfg.alternate;
else
    alternate = 1; % default alternate
end

if isfield(cfg,'jitter')
    jitter = cfg.jitter;
    xmin = 0;
    xmax = 1/(1.1*frequency); % 30 ms
    jitDelays = xmin+rand(1,nTrials)*(xmax-xmin);
else
    jitter = 0; % default do not jitter
end

% Init PTB and all the necessary stuff
% skip internal testing when on mac / windows crappy machine
if ~(meg);Screen('Preference','SyncTestSettings',0.01); else end

% standard PTB init
PsychDefaultSetup(2);
%FOR GENTLE ESCAPE
esc = KbName('ESCAPE');

if meg
    % Setup the Datapixx, both for audio and DIO
    % Open Datapixx, and stop any schedules which might already be running
    % General part
    Datapixx('Open');
    Datapixx('StopAllSchedules');
    %
    % Audio part
    Datapixx('InitAudio');
    Datapixx('SetAudioVolume', [volume 0]);    % Not too loud ('SetAudioVolume', [extVol intVolsca])
    
    % DIO Part
    Datapixx('EnableDinDebounce');                          % Debounce button presses
    Datapixx('SetDinLog');                                  % Log button presses to default address
    Datapixx('StartDinLog');
    Datapixx('RegWrRd');    % Synchronize Datapixx registers to local register cache
    
    
else %not in MEG, use PsychPortAudio for audio and other stuff for triggering
    % here goes another trigger if any
    % e.g. parallel port or labjack if available
    % setup PsychPortAudio,
    InitializePsychSound(1);
    waitForDeviceStart = 1;
    pahandle = PsychPortAudio('Open', [], 1, 1, Fs, 2);
    PsychPortAudio('Volume', pahandle, volume);
end

% this is for stereo sound, for Datapixx
lrMode = 3;


%% Starting ...
% Main loop
%figure;hold all;

%GENTLE ESCAPE
reverseStr='';

for iTrial=1:nTrials
    
    %GENTLE ESCAPE
    % Display the progress
    %percentDone = 100 * iTrial / nTrials;
    %msg = sprintf('Percent done: %3.1f', percentDone); %Don't forget this semicolon
    %fprintf([reverseStr, msg]);
    %reverseStr = repmat(sprintf('\b'), 1, length(msg));
    
    % check every trials the KB
    [keyIsDown,secs,keyCode]=KbCheck; %
    if (keyIsDown==1 && keyCode(esc)) % to break ESC
        Datapixx('Close');
        sca;
        disp('ESCAPE PRESSED: exiting gently ...')
        return
    end;
    
    
    % fixed length ... 80us stimulus
    myBeep = [0 0.5 1 1 1 1 1 1 0.5 0];
    if stereo==1
        myBeep(2,:)=  myBeep(1,:);
    else
        myBeep = [0 0 0 0 0 0 0 0 0 0];
    end
    
    nFrames = max(size(myBeep));
    
    if (alternate==1 && mod(iTrial,2)==0)
        stuffToSend=-myBeep; %invert it!
        inverted =1; %!!!!
    else
        stuffToSend=myBeep;
        inverted =0;
    end
    
    if meg
        Datapixx('SetAudioSchedule', 0, Fs, nFrames, lrMode, 0, nFrames);
        Datapixx('WriteAudioBuffer', stuffToSend, 0);
    else %behav
        PsychPortAudio('FillBuffer', pahandle, stuffToSend);
    end
    %     plot(stuffToSend(1,:));
    %     drawnow;
    % real start of the sound
    
    if inverted %!
        triggerV=3;
    else
        triggerV =1;
    end
    
    if meg
        % Then start the playback ... the audio is sent out only at the next frame after
        Datapixx('SetDoutValues', triggerV);
        if jitter; WaitSecs(jitDelays(iTrial)); else end   
        Datapixx('StartAudioSchedule');
        Datapixx('RegWrVideoSync');
        Datapixx('SetDoutValues', 0);
        Datapixx('StartAudioSchedule');
        Datapixx('RegWrVideoSync');
    else
        if jitter; WaitSecs(jitDelays(iTrial)); else end
        PsychPortAudio('Start', pahandle, 1);%
        %PsychPortAudio('Start', pahandle, 1, [], waitForDeviceStart);
        fprintf('Trigger: %d \n', triggerV );
    end
    
    WaitSecs(1/frequency);
    
    % Stop audio at the end of the trial
    if meg
        Datapixx('StopAudioSchedule');
        Datapixx('RegWrRd');
    else
        PsychPortAudio('Stop', pahandle);
    end
    %tElapsed(iTrial)=toc(tstart);
    %fprintf(repmat('\b',1,15))
    %if (mod(iTrial,10)==1)
    %    clc;
    %    fprintf(1,'\nTrial %d of %d',iTrial-1,nTrials);
    %end
end %trials loop


%% Ending ...

%disp(1/mean(tElapsed));
%
% if meg
%     Screen('FillRect',window,grey ,[320 28 1600 1052]);
% else end
% DrawFormattedText(window, 'End ...', 'center', 'center');
% Screen('Flip',window);

% Wait for keyboard press
%if meg KbStrokeWait; else end

% Show the cursor
ShowCursor;

% Job done
if meg
    Datapixx('Close');
else
    PsychPortAudio('Stop', pahandle);
    PsychPortAudio('Close', pahandle);
end
fprintf('\nDone !\n\n');

% Save and Clean up
% if meg
%     fileName=[cfg.name,'_run',num2str(cfg.block)];
%     save(fileName,'cfg','correctness','RT');
% else end
sca;

end


%% aux functions
function triggerValue(cfg,value)

if cfg.meg == 1
    Datapixx('SetDoutValues', value);
    Datapixx('RegWrRd');
    WaitSecs(0.01); % 20 ms
    Datapixx('SetDoutValues', 0);
    Datapixx('RegWrRd');
    
else
    %some other way to trigger
    fprintf(1,'Trigger %d !\n', value);
end
end


function [button, varargout] = readButtons
% Reads all button-presses currently in RESPONSEPixx buffer (since last
% flush or read). Ignore button releases; return the identity of the last
% button pressed. If no button has been pressed, this returns an empty variable.
% Calling this function wipes the buffer, so if you call it twice in a row,
% the second time will be empty.
% The optional second argument gives the time at which the button-press was
% made, in "Datapixx units". If you want this in seconds since stimulus
% onset, for example, then you need to do something like
% Datapixx('RegWrRd');t0=Datapixx('GetTime')
% [button,t1] = RESPONSEPixx_GetLastButtonPressed;
% reactiontime = t1-t0;
%
% NB This function assumes DATAPixx is Open and generally initialised
% correctly.
%
% Jenny Read, July 2010

% Define user-friendly variable names:
%RESPONSEPixxCodes

Datapixx('RegWrRd');
buttonLogStatus = Datapixx('GetDinStatus');
button = []; % Default is to return empty. Now see if any buttons have been pressed.
timepressed = [];

% Every time a button is pressed, buttonLogStatus.newLogFrames goes up
% by two, once for button-press onset, once for offset.
if (buttonLogStatus.newLogFrames > 0)
    [buttonpresses timestamps] = Datapixx('ReadDinLog');
    % Remove button release events; I only want presses
    %jkeep=[buttonpresses~=RPix_ButtonRelease];
    %buttonpresses = buttonpresses(jkeep);
    %timestamps = timestamps(jkeep);
    if ~isempty(buttonpresses)
        % Only record last press (i.e., user can change their mind)
        button = buttonpresses(end);
        timepressed = timestamps(end);
    end
end
if nargout==2
    varargout{1}=timepressed;
end
end

