function [ sound ] = volume_bayes(cfg, sound)
%VOLUME_STAIRCASE Summary of this function goes here
%   Detailed explanation goes here

ptb = o_ptb.PTB.get_instance;

cfg.question_text = functions.ft_getopt(cfg, 'question_text', 'Haben Sie etwas gehört?');
cfg.yes_text = functions.ft_getopt(cfg, 'yes_text', 'Ja = Roter Knopf');
cfg.no_text = functions.ft_getopt(cfg, 'no_text', 'Nein = Grüner Knopf');

cfg.db_start = functions.ft_getopt(cfg, 'db_start', 40);
cfg.db_step = functions.ft_getopt(cfg, 'db_step', 1);
cfg.db_min = functions.ft_getopt(cfg, 'db_min', 20);
cfg.db_max = functions.ft_getopt(cfg, 'db_max', 120);

cfg.max_trials = functions.ft_getopt(cfg, 'max_trials', 30);

cfg.yes_response = functions.ft_getopt(cfg, 'yes_response', 'yes');
cfg.no_response = functions.ft_getopt(cfg, 'no_response', 'no');

cfg.stim_delay_avg = functions.ft_getopt(cfg, 'stim_delay_avg', 1);
cfg.stim_delay_jitter = functions.ft_getopt(cfg, 'stim_delay_jitter', 0.2);
cfg.additional_post_stim_delay = functions.ft_getopt(cfg, 'additional_post_stim_delay', 0.3);



present_function = @(bayes_cfg) volume_bayes_present(cfg, sound, bayes_cfg);

bayes_cfg = [];
bayes_cfg.thresh = cfg.db_start;

gridu = cfg.db_min:cfg.db_step:cfg.db_max;

[thresh, sc] = ADO_staircase(bayes_cfg, gridu, cfg.max_trials, present_function, 0.5 , false);

sound.db = -thresh;
end

