function trans_mat = create_mid_markov(n_features)

first_row = zeros(1, n_features);
first_row(1) = 1/n_features; % 0.25 repetition

n_remaining = n_features - 2;
p_remaining = (1-first_row(1)) / n_remaining; % eqal prob .375 for next or jump
first_row(2:end-1) = p_remaining;

trans_mat = zeros(n_features);

for idx_row = 1:n_features
  trans_mat(idx_row, :) = circshift(first_row, idx_row-1);
end %for

end
