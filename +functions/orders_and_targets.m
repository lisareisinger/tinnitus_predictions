function subj_data = orders_and_targets(subj_data)
%% get cfg
cfg = exp.init.prepare_cfg; % should be function input (see prepare_subject)


%% Create order
ClockRandSeed();
rng('shuffle');

n_blocks = 6; % should be defined in prepare_cfg
n_subs_perblock = 3; % should be defined in prepare_cfg
n_subs_total = n_blocks*n_subs_perblock;

order_subs = Shuffle(reshape(1:n_subs_total,n_subs_perblock,n_blocks),1); % shuffle between cols (blocks)
order_speaker = Shuffle(repmat([1:n_subs_perblock]',1,n_blocks)); % shuffle within cols (blocks)
order_gender = Shuffle([zeros(n_subs_perblock,n_blocks/2), ones(n_subs_perblock,n_blocks/2)],1); % shuffle between cols (blocks)

%% order of dist indices (see get_stims)
order_dist1_idx = zeros(size(order_speaker));
order_dist2_idx = zeros(size(order_speaker));

for g = 0:1 % gender
    % dist 1
    mask = (order_speaker > 1) & (order_gender == g);
    order_dist1_idx(mask) = 1:nnz(mask);
    % dist 2
    mask = (order_speaker > 2) & (order_gender == g);
    order_dist2_idx(mask) = 1:nnz(mask);
end

%% Create filepaths for each condition
%male
paths.target_male = fullfile(cfg.base_path.home, cfg.sounds.audiobook, 'target', 'male');
paths.dist_male_1 = fullfile(cfg.base_path.home, cfg.sounds.audiobook, 'distractor', 'male', 'dist_1');
paths.dist_male_2 = fullfile(cfg.base_path.home, cfg.sounds.audiobook, 'distractor', 'male', 'dist_2');

%female
paths.target_female = fullfile(cfg.base_path.home, cfg.sounds.audiobook, 'target', 'female');
paths.dist_female_1 = fullfile(cfg.base_path.home, cfg.sounds.audiobook, 'distractor', 'female', 'dist_1');
paths.dist_female_2 = fullfile(cfg.base_path.home, cfg.sounds.audiobook, 'distractor', 'female', 'dist_2');

% loop through paths and get filenames (get rid of extra cell array)
fn = fieldnames(paths);
for f = 1:numel(fn) % loop through folders
    
    d = dir(fullfile(paths.(fn{f}),'*.wav'));
    
    for f2 = 1:size(d,1) % loop through files
        files.(fn{f}){f2} = [d(f2).folder,'/',d(f2).name];
    end

end

%% load trials of audiobook 
[questions, targetwords, distractorwords] = functions.load.load_questions();

%% save
% save order
subj_data.cocktail_party.order.nStim = n_subs_total;
subj_data.cocktail_party.order.order_array = order_subs;
subj_data.cocktail_party.order.speaker = order_speaker;
subj_data.cocktail_party.order.gender = order_gender;
subj_data.cocktail_party.order.dist1_idx = order_dist1_idx;
subj_data.cocktail_party.order.dist2_idx = order_dist2_idx;
subj_data.cocktail_party.order.targetwords = targetwords(order_subs);
subj_data.cocktail_party.order.distractorwords = distractorwords(order_subs);
subj_data.cocktail_party.order.questions = questions(order_subs);

% save paths
subj_data.cocktail_party.paths.target_m = files.target_male(order_subs);
subj_data.cocktail_party.paths.dist_1_m = files.dist_male_1;
subj_data.cocktail_party.paths.dist_2_m = files.dist_male_2;
subj_data.cocktail_party.paths.target_f = files.target_female(order_subs);
subj_data.cocktail_party.paths.dist_1_f = files.dist_female_1;
subj_data.cocktail_party.paths.dist_2_f = files.dist_female_2;

% save (shuffled) trigger (for target books)
subj_data.stims.cocktail_party.trigger.target_book = subj_data.stims.cocktail_party.trigger.target_book(order_subs);


