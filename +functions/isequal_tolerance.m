function [ equality ] = isequal_tolerance( a, b, tol )
%ISEQUAL_TOLERANCE Summary of this function goes here
%   Detailed explanation goes here

if nargin < 3
  tol = eps(a(1));
end %if

equality = abs(a -b) < tol;

end

