function [target_book, dist_1_book, dist_2_book] = get_stims(subj_data, idx)
% get stims according to condition (gender & speaker)
% audiostimulus
% trigger
% informative string?

gender = subj_data.cocktail_party.order.gender(idx);
speaker = subj_data.cocktail_party.order.speaker(idx);
dist1_idx = subj_data.cocktail_party.order.dist1_idx(idx);
dist2_idx = subj_data.cocktail_party.order.dist2_idx(idx);

%% get variable fieldnames depending on gender...
if gender == 0
    g_targ = 'm';
    g_dist = 'f';
else
    g_targ = 'f';
    g_dist = 'm';
end

%% get target_book:

target_book.stim = o_ptb.stimuli.auditory.Wav(subj_data.cocktail_party.paths.(['target_',g_targ]){idx});
target_book.stim.rms = subj_data.stims.volume.audio.rms;
target_book.trigger = subj_data.stims.cocktail_party.trigger.target_book(idx)+gender;

%% get dist_books:

dist_1_book.stim = [];
dist_1_book.trigger = 0;
dist_2_book.stim = [];
dist_2_book.trigger = 0;

% get correct idx (less files)! (see order_and_targets)
if speaker > 1 % we need at least dist_1
    dist_1_book.stim = o_ptb.stimuli.auditory.Wav(subj_data.cocktail_party.paths.(['dist_1_',g_dist]){dist1_idx});
    dist_1_book.stim.rms = subj_data.stims.volume.audio.rms;
    dist_1_book.trigger = subj_data.stims.cocktail_party.trigger.dist_1_book(dist1_idx)+~gender;
    
    if speaker > 2 % we also need dist_2
        dist_2_book.stim = o_ptb.stimuli.auditory.Wav(subj_data.cocktail_party.paths.(['dist_2_',g_dist]){dist2_idx});
        dist_2_book.stim.rms = subj_data.stims.volume.audio.rms;
        dist_2_book.trigger = subj_data.stims.cocktail_party.trigger.dist_2_book(dist2_idx); % no gender info bcs odds add up to evens!
    end
end

end
