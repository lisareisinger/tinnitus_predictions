function [ args ] = obob_cellargs(varargin)
% CIMEC_CELLARGS Returns a cell array with all possible combinations of the
% input parameters provided.
%
% You can provide any number of input arguments. If it is a cell array,
% every entry of it will be combined with every entry of all other cells.
%
% Example:
% args = obob_cellargs({1, 2, 3}, {5, 6})
% args{:}
% ans = 
%     [1]    [5]
% ans = 
%     [2]    [5]
% ans = 
%     [3]    [5]
% ans = 
%     [1]    [6]
% ans = 
%     [2]    [6]
% ans = 
%     [3]    [6]

% Copyright (c) 2014-2016, Thomas Hartmann
%
% This file is part of the obob_ownft distribution, see: https://gitlab.com/obob/obob_ownft/
%
%    obob_ownft is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    obob_ownft is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with obob_ownft. If not, see <http://www.gnu.org/licenses/>.
%
%    Please be aware that we can only offer support to people inside the
%    department of psychophysiology of the university of Salzburg and
%    associates.

nargs = length(varargin);

% checks whether all variable input arguments are wrapped in cells. If not,
% put it in a cell.
% Also, this loop creates the cell array 'alllengths' which holds an
% enumeration of every argument. This is useful for later combination.

alllengths = cell(1, nargs);
for i = 1:nargs
  if ~iscell(varargin{i})
    varargin{i} = {varargin{i}};
  end %if
  alllengths{i} = 1:length(varargin{i});
end %for

% use ndgrid to create all possible combinations.
[allgridcell{1:nargs}] = ndgrid(alllengths{:});
for i = 1:length(allgridcell)
  allgridcell{i} = allgridcell{i}(:);
end %for

for i = 1:length(allgridcell{1})
  args{i} = cell(1, nargs);
  for j = 1:nargs
    args{i}{j} = varargin{j}{allgridcell{j}(i)};
  end %for
end %for
end

