function [ trans_mat ] = create_ordered_markov( n_features )


trans_mat = eye(n_features) * 1/n_features; % matrix with 0.25 along diag

% 0.75 prob for the next tone...
for cur_row = 1:n_features
  idx = cur_row + 1;
  if idx > n_features
    idx = idx - n_features;
  end %if
  
  trans_mat(cur_row, idx) = 1 - 1/n_features;
end %for


end

