function trigger_sequence = get_trigger_sequence(sequence, condition, trigger)

% this function transforms item values to trigger values using the trigger
% info from markov.triggers.
% condition triggers will be added to stimulus triggers to provide info
% about the current entropy level!

trigger_sequence = zeros(size(sequence));
for i = 1:length(sequence)
    cur_stim = sequence(i);
    cur_trigger = trigger.stims(cur_stim) + trigger.condition.(condition);
    trigger_sequence(i) = cur_trigger;
end
end
