function [ equality ] = isequal_rel_tolerance( a, b, tol )
%ISEQUAL_TOLERANCE Summary of this function goes here
%   Detailed explanation goes here

if nargin < 3
  tol = 0.0000001;
end %if

equality = abs(1-(a/b)) < tol ;

end

