function [ sdist ] = stationary_distribution( trans_mat )
%MY_STATIONARY Summary of this function goes here
%   Detailed explanation goes here

[v, d] = eig(trans_mat', 'vector');

[~, idx] = min(abs(d-1));

v_new = abs(v(:, idx)');

sdist = v_new ./ sum(v_new)';

end

