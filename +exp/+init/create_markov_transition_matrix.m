function trans_mats = create_markov_transition_matrix(cfg)

%loads all markov trans_mats

n_features = cfg.markov.n_features;
trans_mats = [];

trans_mats.ordered = functions.create_ordered_markov(n_features);
trans_mats.random = functions.create_random_markov(n_features);                  
trans_mats.mid = functions.create_mid_markov(n_features);

end
