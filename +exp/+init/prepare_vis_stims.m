function subj_data = prepare_vis_stims(subj_data)

%% Symbols
visual.fixcross = o_ptb.stimuli.visual.Text('+');
visual.fixcross.size = 250;

%% Instruction
visual.instruction.start_text = o_ptb.stimuli.visual.Text('Wir beginnen in Kuerze mit der Testung. \n\n Bitte bewegen Sie sich nicht.');
visual.instruction.start_text.size=70;

%% Break betw. subblocks
visual.break_text = o_ptb.stimuli.visual.Text('Zum Weitermachen bitte den gelben Knopf druecken');
visual.break_text.size=70;

%% End of block
visual.blockend_text = o_ptb.stimuli.visual.Text(sprintf('Das Experiment ist fertig.'));
visual.blockend_text.size=70;

subj_data.stims.visual = visual;
