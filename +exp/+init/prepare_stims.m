function stims = prepare_stims(cfg)


%% Markov 
for i=1:length(cfg.markov.sounds.freqs)
  cur_freq = cfg.markov.sounds.freqs(i);
  stims.markov.audio{i} = o_ptb.stimuli.auditory.Sine(cur_freq, cfg.markov.sounds.stim_length);
  stims.markov.audio{i}.apply_cos_ramp(cfg.markov.sounds.fade);
end %for

stims.markov.trans_mats = exp.init.create_markov_transition_matrix(cfg);

%% Trigger for markov
stims.markov.trigger = [];

stims.markov.trigger.stims = [16 32 64 128];

stims.markov.trigger.condition.ordered = 1;
stims.markov.trigger.condition.random = 2;

% note that triggers will be added for every stimulus to provide info about
% current entropy level! (e.g. F440 during random = 17)

end
