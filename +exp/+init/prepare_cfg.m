function cfg = prepare_cfg()
%PREPARE_CFG Summary of this function goes here
%   Detailed explanation goes here
cfg = [];
cfg.data_path = 'data_pilot';


% path to experiment at home change only this one
% %path@cdk
cfg.base_path.home = '/home/schmidtfa/experiments/SBGHearingMeg/';

%path @Fabi
%cfg.base_path.home = '/Users/b1059770/git/SBGHearingMeg/';

%path @Juli
%cfg.base_path.home = '/home/juliane/Documents/Programmieren/MATLAB/Psychtoolbox/listening_paradigm/audiobook_masking';

%Path to all soundfiles
%cfg.sounds.mcdermot = '/soundfiles/naturalsounds165/';

%Path to all audiobook
cfg.sounds.audiobook = '/soundfiles';

% volume staircase
cfg.add_db = 40;

%% Settings for markov
cfg.markov.n_features = 4;
cfg.markov.min_prob = 0.05; % ??
cfg.markov.iti = 1/3;
cfg.markov.trials_per_block = 1500;


base_freq = 440;
fourth_ratio = 4/3;

freqs = zeros(1, 4);
for idx_freq = 1:length(freqs)
    freqs(idx_freq) = base_freq * (fourth_ratio^(idx_freq-1));
end %for

cfg.markov.sounds.freqs = freqs;
cfg.markov.sounds.stim_length = 0.1;
cfg.markov.sounds.fade = 5e-3;

cfg.triggers.stims = [16 32 64 128];

cfg.triggers.condition.ordered = 1;
cfg.triggers.condition.random = 2;

end

