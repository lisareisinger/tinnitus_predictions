function ptb_config = config_ptb()
%% init paths....

%@Fabi
%addpath('/Users/b1059770/Psychtoolbox/o_ptb');
%o_ptb.init_ptb('/Users/b1059770/Psychtoolbox/Psychtoolbox-3'); % initialize the PTB

% %@cdk
addpath('/home/schmidtfa/experiments/o_ptb/');
o_ptb.init_ptb('/home/schmidtfa/experiments/Psychtoolbox-3/'); % initialize the PTB

commandwindow; % opens command window or selects it if already open...
%% create ptb_config
ptb_config = o_ptb.PTB_Config();
ptb_config.window_scale = 0.3;
ptb_config.datapixxaudio_config.buffer_address = 552000000;
ptb_config.datapixxaudio_config.freq = 44100;
ptb_config.flip_horizontal = false;
ptb_config.fullscreen = false;
ptb_config.skip_sync_test = true;
ptb_config.hide_mouse = false;
ptb_config.force_datapixx = false;
ptb_config.internal_config.blend_function = {'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA'};
% ptb_config.internal_config.trigger_subsystem = @th_ptb.subsystems.trigger.Dummy; %to make it run on debian
ptb_config.datapixxaudio_config.volume = [1 0.001];

ptb_config.defaults.text_size = 70;
ptb_config.defaults.fixcross_size = 240;

ptb_config.internal_config.use_decorated_window = false;

% button config for audiobook masking % add config for meg
ptb_config.keyboardresponse_config.button_mapping('next') = KbName('space');
ptb_config.datapixxresponse_config.button_mapping('next') = ptb_config.datapixxresponse_config.Yellow;
ptb_config.keyboardresponse_config.button_mapping('measure') = KbName('Return');

%use this only when you run it for real in the CDK
ptb_config.real_experiment_sbg_cdk(true);

end

