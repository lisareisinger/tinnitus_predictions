%% CocktailParty

function audio_conti(subject_id)
commandwindow;
%% get cfg
cfg = exp.init.prepare_cfg;

%% init ptb
ptb_config = exp.init.config_ptb;
ptb = exp.init.init_ptb(ptb_config);

%% load subject_data
load(fullfile(cfg.data_path, subject_id), 'subj_data');

%% Init screen and responses
ptb.setup_screen;
ptb.setup_response;

%% TODO: Load the visual stimuli
subj_data = exp.init.prepare_vis_stims(subj_data);

%% Play instructions

ptb.draw(subj_data.stims.visual.instruction.start_text);
ptb.flip();
KbWait();
timestamp = ptb.flip();
WaitSecs('UntilTime', timestamp + 1)

%% Preload audio

ptb.draw(o_ptb.stimuli.visual.Text('Lade Hörbuch'));
WaitSecs(0.1);
ptb.flip();

%% get audiobooks according to current gender and speaker condition
cur_book = char('hdr_all_mono.wav');
target_book.stim = o_ptb.stimuli.auditory.Wav(fullfile(cfg.base_path.home, '/soundfiles/', cur_book));
target_book.stim.rms = subj_data.ambientrms;
target_book.trigger = 1;

%% start sounds
% prepare audio and trigger...
ptb.prepare_audio(target_book.stim);
ptb.prepare_trigger(1) %trigger start s1
ptb.prepare_trigger(99, target_book.stim.duration, true)

% draw fixcross
ptb.draw(subj_data.stims.visual.fixcross);
WaitSecs(0.1);
ptb.flip();

ptb.schedule_audio;
ptb.schedule_trigger;

ptb.play_without_flip;
WaitSecs(target_book.stim.duration + 2);

%% Info that the block has ended
ptb.draw(subj_data.stims.visual.blockend_text);
ptb.flip();
WaitSecs(2)

sca