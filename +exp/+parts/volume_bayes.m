function volume_bayes(subject_id)
% do volume staircase to get individual hearing threshold
commandwindow;
%% get cfg
cfg = exp.init.prepare_cfg;

%% init ptb
ptb_config = exp.init.config_ptb;
ptb = exp.init.init_ptb(ptb_config);

addpath(genpath('external/VBA-toolbox/'));
addpath('external/gaetan_ADO/');

%% load subject_data
load(fullfile(cfg.data_path, subject_id), 'subj_data');

%% check if we already did it...
% if subj_data.volume_staircase_done
%   error('Volume Staircase already done.');
% end %if

%% start screen and response
ptb.setup_screen;
ptb.setup_response;

%% tell subject to wait...
wait_text = o_ptb.stimuli.visual.Text('Bitte warten Sie. Es geht gleich los');
ptb.draw(wait_text);
ptb.flip();

KbWait();

%% call volume_staircase...
cfg_vs = [];
o_ptb_helper.volume_bayes(cfg_vs, subj_data.stims.markov.audio{end}); %use highest markov freq

%% process volume staircase...
fprintf('Threshold is at %fdB\n', subj_data.stims.markov.audio{end}.db);

subj_data.stims.markov.audio{end}.amplify_db(cfg.add_db);
new_rms = subj_data.stims.markov.audio{end}.rms;

% apply new rms to markov stims
for i = 1:length(subj_data.stims.markov.audio)
  subj_data.stims.markov.audio{i}.rms = new_rms;
end %for

% apply new rms to cocktail party audio files
subj_data.stims.volume.audio.rms = new_rms;
%% save it
subj_data.volume_staircase_done = true;

save(fullfile(cfg.data_path, subject_id), 'subj_data');

sca;

end
