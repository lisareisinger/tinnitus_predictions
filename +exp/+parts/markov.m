function markov(subject_id, block_nr)
%% get cfg
cfg = exp.init.prepare_cfg;

%% init ptb
ptb_config = exp.init.config_ptb;
ptb = exp.init.init_ptb(ptb_config);

%% load subject_data
load(fullfile(cfg.data_path, subject_id));

%% check if block has already been done...
if block_nr <= subj_data.markov_block_done
    error('You already did that kind of block');
end %if

% % %% check if resting state was done
% % if ~subj_data.resting_done
% %     error('Resting run was not yet done...');
% % end %if

%% check if the block number is too high...
if block_nr > subj_data.markov_block_done + 1
    fprintf('\n\n\nWARNING!!! You are skipping a block!! This is very probably NOT WHAT YOU WANT!\n');
    fprintf('If you really want to continue, press the "y" button now.\n\n\n\n');
    
    [~, key_pressed] = KbWait();
    if find(key_pressed) ~= KbName('y')
        return;
    end %if
end %if

%% starting...
fprintf('Please wait while all stimuli are loaded. This might take a while...\n');

trigger_sequence = zeros(length(subj_data.markov.conditions), cfg.markov.trials_per_block/length(subj_data.markov.conditions));
for i = 1:length(subj_data.markov.conditions)
    sequence = exp.parts.helper.prepare_markov_sound_chain(cfg, subj_data.stims, subj_data.markov.conditions{block_nr,i}, cfg.markov.trials_per_block/length(subj_data.markov.conditions));
    trigger_sequence(i,:) = functions.get_trigger_sequence(sequence, subj_data.markov.conditions{block_nr,i}, subj_data.stims.markov.trigger);
end
trigger_sequence = reshape(trigger_sequence',1,numel(trigger_sequence));

fprintf('\n\nOk, we are starting in 10 seconds. Get that movie running now!\n\n');
WaitSecs(10); % wait 10 seconds

%% submit first part
% (needs to be done in two parts for datapixx)
run_time = exp.parts.helper.submit_markov_sound_chain(cfg, subj_data.stims, trigger_sequence, true);
ptb.schedule_audio;
ptb.schedule_trigger;

ptb.play_without_flip;

% wait until done... (for Datapixx)
WaitSecs(run_time);

%% submit second part...
run_time = exp.parts.helper.submit_markov_sound_chain(cfg, subj_data.stims, trigger_sequence, false);
ptb.schedule_audio;
ptb.schedule_trigger;

ptb.play_without_flip;

% wait until done... (for Datapixx)
WaitSecs(run_time);

%% save that everything was done and store it in the subj_data file
fprintf('We should be done now...\n');

subj_data.markov_block_done = block_nr;
subj_data.markov_sequences{block_nr} = sequence;

fprintf('\n\nPlease wait while everything is saved!\n\n');
save(fullfile(cfg.data_path, subject_id), 'subj_data');
fprintf('\n\nSaving done!\n\n');

end
