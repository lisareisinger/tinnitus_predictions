function prepare_audio(ptb, obj, varargin)
% wrapper aroung ptb.prepare_audio to ignore empty input

if isobject(obj)
    ptb.prepare_audio(obj, varargin{:})
end

end