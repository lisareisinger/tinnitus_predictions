function matchambientlevel(subject_id)

commandwindow;
%% CHECK WHETHER RMS HAS ALREAD BEEN CALCULATED
%% get cfg
cfg = exp.init.prepare_cfg;
%% load subject_data
load(fullfile(cfg.data_path, subject_id), 'subj_data');
exp.parts.helper.show_loudness_warn_dlg('middle')

if ~isfield(subj_data, 'ambientrms')
    fprintf('Loading Audio ...\n')
    [storyWav, Fs] = audioread('./soundfiles/schnitzler_fremde.mp3');
    fprintf('Done.\n')

    
    %% init...
    ptb_config = exp.init.config_ptb;
    ptb_config.real_experiment_sbg_cdk(false);
    ptb = o_ptb.PTB.get_instance(ptb_config);
    ptb.setup_audio();
    
    %%
    
    %     wait_text = o_ptb.stimuli.visual.Text('Bitte Warten ...!');
    %     ptb.draw(wait_text);
    %     ptb.flip();
    %
    %     KbWait;
    
    %% Generate random 10 sec segments
    
    winsecs=10; % 10 second segments
    startindx=[1:Fs*winsecs:length(storyWav)-Fs*winsecs];
    randseq=randperm(length(startindx));
    
    %%
    new_rms=.013;
    ii=1;
    while ~isempty(new_rms)
        
        %         fix_cross = o_ptb.stimuli.visual.FixationCross();
        %         ptb.draw(fix_cross);
        %         ptb.flip();
        
        snipp_storyWav=storyWav(startindx(randseq(ii)):startindx(randseq(ii))+Fs*winsecs);
        snipp_storyWav = o_ptb.stimuli.auditory.FromMatrix(snipp_storyWav', Fs);
        snipp_storyWav.rms = new_rms;
        
        %%
        
        ptb.prepare_audio(snipp_storyWav);
        ptb.schedule_audio;
        
        now_time=GetSecs();
        ptb.play_without_flip;
        
        
        WaitSecs('UntilTime', now_time + snipp_storyWav.duration + 2);
        %         wait_text = o_ptb.stimuli.visual.Text('Wie angenehm war die Lautstärke?');
        %
        %         ptb.draw(wait_text);
        %         ptb.flip();
        %
        fprintf('Current RMS: %f.\n', snipp_storyWav.rms(1));
        
        new_rms=input('New RMS (hit return to quit): ');
        
        while new_rms/snipp_storyWav.rms(1) > 2
            fprintf('Too much increase in one step!\n');
            new_rms=input('New RMS (hit return to quit): ');
        end
        
        
        %ADD SECURITY THAT IF STEP TO BIG TO HAVE NEW PROMP
        
        KbWait;
        ii=ii+1;
        
    end
     
    subj_data.ambientrms = snipp_storyWav.rms(1);
    
    
    % apply new rms to markov stims
    for i = 1:length(subj_data.stims.markov.audio)
        subj_data.stims.markov.audio{i}.rms = subj_data.ambientrms;
    end %for

    
    save(fullfile(cfg.data_path, subject_id), 'subj_data');
    sca;
    
else
    fprintf('Ambient RMS has already been determined. You may continue!\n');
end

end
