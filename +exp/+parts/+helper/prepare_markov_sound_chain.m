function sequence = prepare_markov_sound_chain(cfg, stims, condition, n_stims)

% the markov chain (with length n_stims) is generated from transition
% probabilities (hidden to the observer, as we have defined them) and
% emission probabilities (= probabilities of the observations themselves)

% EXAMPLE: suppose we have only 2 states = (tones)
%{
p_trans = [0, 1;... %from state 1 it goes always to state 2
           1, 0] %(and vice versa)
p_em = [0.9, 0.1;... %BUT if state 1 is chosen there is still a small prob that state 2 will be emitted
        0.1, 0.9] %(and vice versa)
markov_chain = hmmgenerate(10,p_trans,p_em)
%}
% the generated sequence could look like this:
% 2     1     2     1     1     1     2     1     2     1

% in our case we always want to emit the given state with 100% prob...
sequence = hmmgenerate(n_stims, stims.markov.trans_mats.(condition), eye(cfg.markov.n_features));

end

