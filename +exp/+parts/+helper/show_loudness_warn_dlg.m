function show_loudness_warn_dlg(loudness)
%SHOW_SFREQ_WARN_DLG Summary of this function goes here
%   This function ensures that the students picked the right project and
%   the right sampling rate for their recording
volume.high = 'high';
volume.middle = 'middle';

%for the sampling rate
switch loudness
    case 'high'
        needs_loudness = 'high';
    case 'middle'
        needs_loudness = 'middle';
    otherwise
        error('Invalid loudness level selected')
end

needs_volume = volume.(needs_loudness);   

dlg_title = 'Please verify the project!';
dlg_text = sprintf(['Please look to the left NOW and CHECK that:\n' ...
                    'The volume is set to %s\n\n' ...
                    'Then click on the necessary loudness setting for this part of the experiment:\n'], ...
                    needs_volume);

all_fields = fieldnames(volume);
all_fields{end+1} = 'whatever';

all_fields = Shuffle(all_fields);

choice = questdlg(dlg_text, dlg_title, all_fields{1}, all_fields{2}, ...
  all_fields{3}, all_fields{1});

if ~strcmp(choice, needs_loudness)
  error('You chose the wrong loudness setting')
end %if


end

