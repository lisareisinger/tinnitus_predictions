function run_time = submit_markov_sound_chain(cfg, stims, trigger_sequence, first_half)

ptb = o_ptb.PTB.get_instance;

n_stims = length(trigger_sequence);
half_n_stims = round(n_stims/2);
start_delay = 0;

if first_half
  indices = 1:half_n_stims;
else
  indices = (half_n_stims+1):n_stims;
end %if

for ii = indices
  cur_trigger = trigger_sequence(ii);
  cur_item = floor(log(cur_trigger)/log(2))-3;
  
  i = ii - indices(1) + 1;
  
  ptb.prepare_audio(stims.markov.audio{cur_item}, start_delay + i*cfg.markov.iti, i>1);
  ptb.prepare_trigger(cur_trigger, start_delay + i*cfg.markov.iti, i>1);
end %for

run_time = start_delay + (length(indices)+1) *cfg.markov.iti;

end

