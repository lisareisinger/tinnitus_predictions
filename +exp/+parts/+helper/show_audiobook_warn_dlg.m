function show_audiobook_warn_dlg(audiobook)
%SHOW_SFREQ_WARN_DLG Summary of this function goes here
%   This function ensures that the students picked the right project and
%   the right sampling rate for their recording
book.crusoe = 'crusoe';
book.lenin = 'lenin';

%for the sampling rate
switch audiobook
    case 'lenin'
        needs_book = 'GoodbyeLenin';
    case 'crusoe'
        needs_book = 'RobinsonCrusoe';
    otherwise
        error('Invalid audiobook selected')
end

needs_audiobook = book.(needs_book);   

dlg_title = 'Please verify the project!';
dlg_text = sprintf(['Please check whether you are using the right audiobook:\n' ...
                    'If its the first time you are testing the subject it should be %s\n\n' ...
                    'Then choose whether current sampling rate is:\n'...
                    'high_freq = 10000Hz\n'...
                    'or\n'...
                    'low_freq = 1000Hz'], ...
                    needs_audiobook);

all_fields = fieldnames(book);
all_fields{end+1} = 'whatever';

all_fields = Shuffle(all_fields);

choice = questdlg(dlg_text, dlg_title, all_fields{1}, all_fields{2}, ...
  all_fields{3}, all_fields{1});

if ~strcmp(choice, needs_sampling)
  error('You chose the wrong Project')
end %if


end

