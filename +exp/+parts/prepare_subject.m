function prepare_subject(subject_id)

cfg = exp.init.prepare_cfg;
if ~exist(cfg.data_path)
    mkdir(cfg.data_path);
end %if

if exist(fullfile(cfg.data_path, [subject_id '.mat']))
    fprintf('The Subject is already prepared. You can go on\n');
    return;
end %if

subj_data.stims = exp.init.prepare_stims(cfg);

%Resting
subj_data.resting_done = false;

%ABR
subj_data.abr_done = false;

%% Markov
all_conditions = fieldnames(subj_data.stims.markov.trigger.condition);
cond_mat = cell(2,length(all_conditions)+1);

%%%%%%%%%%%%%%%%%%%%%%% old (within-)design %%%%%%%%%%%%%%%%%%%%
for i = 1:length(all_conditions)
    cond_mat(i,1:2) = Shuffle(all_conditions);
end
cond_mat(:,3) = Shuffle(all_conditions);

%%%%%%%%%%%%%%%%%%%%% new (between-) design %%%%%%%%%%%%%%%%%%%%

subj_data.markov.conditions = cond_mat;
subj_data.markov_block_done = 0;

%Audiobook
subj_data.cocktail_party_done = false;
subj_data.cocktail_party_block_done = 0;

%Prepare orders and targets
%subj_data = functions.orders_and_targets(subj_data);

save(fullfile(cfg.data_path, subject_id), 'subj_data');
end

