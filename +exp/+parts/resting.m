function resting(subject_id)
%RESTING Summary of this function goes here
%   Detailed explanation goes here
%% get cfg
cfg = exp.init.prepare_cfg;

%% init ptb
ptb_config = exp.init.config_ptb;
ptb = exp.init.init_ptb(ptb_config);

%% load subject_data
load(fullfile(cfg.data_path, subject_id));

%% check whether resting has already been done
if subj_data.resting_done
  fprintf('\n\n\nWARNING!!! You already did the resting state run!! This is very probably NOT WHAT YOU WANT!\n');
  fprintf('If you really want to continue, press the "y" button now.\n\n\n\n');
  
  [~, key_pressed] = KbWait();
  if find(key_pressed) ~= KbName('y')
    return;
  end %if
end

%% init screen
ptb.setup_screen();

%% Show instructions
inst_text = o_ptb.stimuli.visual.Text('Einen Moment. Es geht gleich los...');
ptb.draw(inst_text);
ptb.flip();

KbWait(-1);

%% show fixation cross...
fixcross = o_ptb.stimuli.visual.Text('+');
fixcross.size = 250;

ptb.draw(fixcross)
ptb.flip()

%% Wait for 5 minutes
WaitSecs(5*60+10);

%% Goodbye
ptb.deinit();
subj_data.resting_done = true;
save(fullfile(cfg.data_path, subject_id), 'subj_data');

