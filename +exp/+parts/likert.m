function response = likert(ptb, question)

%% get cfg
cfg = exp.init.prepare_cfg();

%% init ptb
% ptb_config = exp.init.config_ptb();
% ptb = exp.init.init_ptb(ptb_config);
% 
% %% init screen
% ptb.setup_screen();
% ptb.setup_response;

%% get txt
question = o_ptb.stimuli.visual.Text(question);
txt_lowest = o_ptb.stimuli.visual.Text('gar nicht');
txt_highest = o_ptb.stimuli.visual.Text('sehr');

%% scale stuff...
unselected = cell(1,5);
for i = 1:length(unselected)
    unselected{i} = o_ptb.stimuli.visual.Image(fullfile(cfg.base_path.home, '/stims/unselected.png'));
    unselected{i}.scale(cfg.likert.image_scale)
end
selected = o_ptb.stimuli.visual.Image(fullfile(cfg.base_path.home,'/stims/selected.png'));
selected.scale(cfg.likert.image_scale)

click = o_ptb.stimuli.visual.Image(fullfile(cfg.base_path.home,'/stims/click.png'));
click.scale(cfg.likert.image_scale)

%% align & draw stuff
whereamI = 3;
for i = 1:length(unselected)
    
    unselected{i}.move(cfg.likert.position{i}(1), cfg.likert.position{i}(2))
    ptb.draw(unselected{i})
    
end

selected.move(cfg.likert.position{whereamI}(1), cfg.likert.position{whereamI}(2))
ptb.draw(selected)

question.move(cfg.likert.question(1), cfg.likert.question(2))
ptb.draw(question)
txt_lowest.move(cfg.likert.txt_lowest(1), cfg.likert.txt_lowest(2))
ptb.draw(txt_lowest)
txt_highest.move(cfg.likert.txt_highest(1), cfg.likert.txt_highest(2))
ptb.draw(txt_highest)
ptb.flip()

%% wait for button response...
button = [];
while true
    button = ptb.wait_for_keys({'right', 'left', 'choose'});
    
    if strcmp(button, 'right')
        if whereamI < 5 % = max
            whereamI = whereamI +1;
            horizontal_move = cfg.likert.move;
        else
            horizontal_move = 0;
        end
    elseif strcmp(button, 'left')
        if whereamI > 1 % = min
            whereamI = whereamI -1;
            horizontal_move = cfg.likert.move*-1;
        else
            horizontal_move = 0;
        end
    else
        response = whereamI;
        break % choose selected option
    end %if
    
    for i = 1:length(unselected)
        ptb.draw(unselected{i})
    end % for
    selected.move(horizontal_move, 0)
    ptb.draw(selected)
    
    ptb.draw(question)
    ptb.draw(txt_lowest)
    ptb.draw(txt_highest)
    
    ptb.flip()
    WaitSecs(0.3) % otherwise datapixx takes last response again...
    
end % while

%% draw feedback?
for i = 1:length(unselected)
    ptb.draw(unselected{i})
end
click.move(cfg.likert.position{response}(1), cfg.likert.position{response}(2))
ptb.draw(click)
ptb.draw(question)
ptb.draw(txt_lowest)
ptb.draw(txt_highest)
ptb.flip()
WaitSecs(0.5)

for i = 1:length(unselected)
    ptb.draw(unselected{i})
end
ptb.draw(selected)
ptb.draw(question)
ptb.draw(txt_lowest)
ptb.draw(txt_highest)
ptb.flip()
WaitSecs(0.5)
ptb.flip()
end