%% RUNNING TONES DECODING ON CLUSTER (all_subjects):
% add paths & init packages:
clc
clear all
addpath('/mnt/obob/obob_ownft');

cfg = [];
cfg.package.plus_slurm = true;
obob_init_ft(cfg);

addpath('/mnt/obob/staff/jschubert/myfuns'); % must be set after obob_init_ft
addpath('/mnt/obob/staff/lreisinger/tinnitus/clusterjobs');

% create struct for cluster job
cfg = [];
cfg.mem = '16G';
cfg.request_time = 3000;
condor_struct = obob_slurm_create(cfg);

% data paths
ica_dir = '/mnt/obob/staff/lreisinger/tinnitus/data/ICA/ICA_RR';
outdir = '/mnt/obob/staff/lreisinger/tinnitus/data/markov_longepochs/';
cond = readtable('/mnt/obob/staff/lreisinger/tinnitus/others/tinnitus.csv');

if ~isfolder(outdir)
    mkdir(outdir)
end

% get subjects
% study_acronym = 'fs_sbg_hearing';
% all_subjects = functions.js_subjectsfromsinuhe(study_acronym);
all_subjects = cond.subject_id';

%% decisions for preprocessing
cfg_ppp = [];
cfg_ppp.block = 'markov_blk';
cfg_ppp.channels = 'MEG';
cfg_ppp.lp = 30; % choose Lp-Freq or 'no'
cfg_ppp.res = 100; % choose sample frequency (consider Nyquist!)
cfg_ppp.hp = 0.1; % choose Hp-Freq or 'no'
cfg_ppp.epoch.presecs = 500e-3; % epoch X secs starts pre-stimulus onset
cfg_ppp.epoch.postsecs = 800e-3; % epoch X secs ends post-stimulus onset


%% send to cluster
condor_struct = obob_slurm_addjob_cell(condor_struct, 'c_markov_preproc', ...
  all_subjects, study_acronym, ica_dir, outdir, cfg_ppp);
obob_slurm_submit(condor_struct);

