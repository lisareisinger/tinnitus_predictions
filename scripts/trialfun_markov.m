function [trl, event] = trialfun_markov(cfg)

% trialfun_markov creates a field "trialinfo":
%
% (col 1) trialinfo for 'entropy' = 1 = ordered, 2 = random
% (col 2) trialinfo for 'frequency' = [1 2 3 4];
%
% this function calls functions from fieldtrip (make sure you have it initialized)!


% thx to the fucking 10kHz we have to loop and append the whole shit...
% (cfg.dataset should be an array with all split-file paths)
all_files = cfg.dataset;

if iscell(all_files)
    all_splits = cell(length(all_files),1);
else
    all_splits = 1;
end

trigger = [];
sample = [];
last_sample = 0;
for i = 1:length(all_splits)
    
    if iscell(all_files)
        fname = all_files{i};
    else
        fname = all_files;
    end
    
    hdr = ft_read_header(fname);
    event = ft_read_event(fname);
    
    % search for "trigger" events
    trigger_split = [event(strcmp('Trigger', {event.type})).value]';
    sample_split  = [event(strcmp('Trigger', {event.type})).sample]';
    
    trigger = [trigger; trigger_split];
    
    sample_split = sample_split + last_sample;
    last_sample = last_sample + hdr.nSamples;
    sample = [sample; sample_split];
end


% determine the number of samples before and after the trigger
pretrig  = -round(cfg.trialdef.prestim  * hdr.Fs);
posttrig =  round(cfg.trialdef.poststim * hdr.Fs);

entropy_info = bitand(1,trigger);
entropy_info(entropy_info == 0) = 2;

trg_frequ = [16 32 64 128];
for i = 1:length(trg_frequ)
    trigger(bitand(trg_frequ(i),trigger)~= 0) = i;
end

trl = [];
for itrial = 1:length(trigger)% loop through all trigger
    freq_info = trigger(itrial);
    trlbegin = sample(itrial) + pretrig;
    trlend   = sample(itrial) + posttrig;
    offset   = pretrig;
    
    newtrl   = [trlbegin trlend offset entropy_info(itrial) freq_info];
    
    trl      = [trl; newtrl];
end

end