library(ggplot2)
library(scales)

df_study2 <-  read.csv("/home/lisa/Documents/Doktorat/registered report/analysis/rr_tinnitus/bf_study2.csv")
df_study1 <-  read.csv("/home/lisa/Documents/Doktorat/registered report/analysis/rr_tinnitus/bf_study1.csv")
time <- seq(-0.5, 0.8, by=0.01)
BF10 <- matrix(t(df_study1), ncol=1, nrow=ncol(df_study1)*nrow(df_study1), byrow=F)
df_study1 <- data.frame(BF10)

BF10 <- matrix(t(df_study2), ncol=1, nrow=ncol(df_study2)*nrow(df_study2), byrow=F)
df_study2 <- data.frame(BF10)

df_study2$time <- time
df_study1$time <-time
df_study2$group <- 'Study 2'
df_study1$group <- 'Study 1'
df_both <- rbind(df_study2,df_study1)

ggplot(df_both, aes(x = time, y =BF10, color = group))+
  geom_line() +
  scale_y_continuous(trans='log10') +
  ylab('Bayes Factor (Tinnitus vs Control) - log scaled') +
  geom_hline(yintercept=3, linetype="dashed", color = "black")+
  geom_hline(yintercept=1, linetype="dashed", color = "black")+
  geom_hline(yintercept=10, linetype="dashed", color = "black")+
  theme_classic()


## trial vergleich

df_200 <-  read.csv("/home/lisa/Documents/Doktorat/registered report/analysis/rr_tinnitus/bf_200.csv")
df_750 <-  read.csv("/home/lisa/Documents/Doktorat/registered report/analysis/rr_tinnitus/bf_750.csv")
df_1500 <-  read.csv("/home/lisa/Documents/Doktorat/registered report/analysis/rr_tinnitus/bf_1500.csv")



df_200 <- df_200[,53:183]
df_750 <- df_750[,53:183]
df_1500 <- df_1500[,53:183]

time <- seq(-0.5, 0.8, by=0.01)

BF10 <- matrix(t(df_200), ncol=1, nrow=ncol(df_200)*nrow(df_200), byrow=F)
df_200 <- data.frame(BF10)

BF10 <- matrix(t(df_750), ncol=1, nrow=ncol(df_750)*nrow(df_750), byrow=F)
df_750 <- data.frame(BF10)

BF10 <- matrix(t(df_1500), ncol=1, nrow=ncol(df_1500)*nrow(df_1500), byrow=F)
df_1500 <- data.frame(BF10)


df_200$time <- time
df_750$time <- time
df_1500$time <- time

df_200$group <- '200'
df_750$group <- '750'
df_1500$group <- '1500'
df_study1$group <- 'all trials'

df_all <- rbind(df_200,df_750, df_1500, df_study1)

ggplot(df_all, aes(x = time, y =BF10, color = group))+
  geom_line() +
  scale_y_continuous(trans='log10') +
  ylab('Bayes Factor (Tinnitus vs Control) - log scaled') +
  theme_classic()
