%% plot time generalisation training RdSND testing SND with  stat
clear all; close all; restoredefaultpath; matlabrc;
addpath /mnt/obob/staff/gdemarchi/git/fieldtrip-donders
ft_defaults

%%
addpath(genpath('/mnt/obob/staff/gdemarchi/git/MVPA-Light/'));
startup_MVPA_Light;

addpath /mnt/obob/staff/gdemarchi/mattools/;
addpath /mnt/obob/staff/gdemarchi/git/export_fig/;
addpath /mnt/obob/staff/gdemarchi/DataAnalysis/tinnitusMarkov/decoding/functions/

fileDir = (['/mnt/obob/staff/gdemarchi/data/tinnitusMarkov/decoding/TG_trSNDteSND/']);

%% TINNITUS PATIENTS

subjListP =  {
'181108ATLN', %	Tinnitus
'171128BASH', % Tinnitus
'180516CRGL', %	Tinnitus
'180514PTBU', %	Tinnitus
'171215SBOE', %	Tinnitus
'180810GBSR', %	Tinnitus
'181114EILC', %	Tinnitus
'180516GASA', %	Tinnitus
'190304EMLP', %	Tinnitus
'181109CRBU', %	Tinnitus
'170704FAZC', %	Tinnitus
'180626PISH', %	Tinnitus
'170418cris', %	Tinnitus
'180424ANME', %	Tinnitus
'170928AOHM', %	Tinnitus
'180718RSAT', %	Tinnitus
'171020SSSH', %	Tinnitus
'180718ANFI', %	Tinnitus
'180306ZHCU', %	Tinnitus
'180417CRAZ', %	Tinnitus
'190116CASI', %	Tinnitus
'171020CRRI', %	Tinnitus
'170505WLTI', %	Tinnitus
% '180619MRLI', %	Tinnitus % not in group c.
'170428MRBR', %	Tinnitus
'180514MRTE', %	Tinnitus
% '171128BRKE', %	Tinnitus % not in group c.
% '180525MRPA', %	Tinnitus % not in group c.
% '180626EIKN', %	Tinnitus % not in group c. 
% '171020EIGA', %	Tinnitus % not in group c.
% '170704HDEM', %	Tinnitus % not in group c.
% %'190325HDHB', %	Tinnitus % not in group c.
%'170609JHSH', %	Tinnitus % not in group c.
%'180619ANGU', %	Tinnitus % not in group c.
 };


% the same for  %% CONTROLS, 25 as the Tinnitus target groups
subjListC =  {
'170616SBPE', %	Control
'170624BTKC', %	Control
'170624EEHB', %	Control
'170624GNTA', %	Control
'170511MNSU', %	Control
'170517KTAD', %	Control
'170511CRBC', %	Control
'170718GDZN', %	Control
'170718SLBR', %	Control
'170906PNRK', %	Control
'190119KTML', %	Control
'181109JSHP', %	Control
'190125MNPR', %	Control
'190112MRSH', %	Control
'190124JSJB', %	Control
'180607RSAS', %	Control
'190107MRKB', %	Control
'170511GBHL', %	Control
'170906KRHR', %	Control
'181218PIKN', %	Control
'181017MRPC', %	Control
'190112MRHC', %	Control
'190119CRLN', %	Control
'190129MRBN', %	Control
'180626ANMY', %	Control
  };
%%
close all;
clear fake* TG_acc*  data* comp* tmptg*  fNamePart fileToRead  curFile bad* MIA*
Fs = 100;
numP = length(subjListP);
numC = length(subjListC);

subSamples = {'100','200','500','750', '1000', '1500', '2000', 'all'};

for iSub = 1:length(subSamples)    
    fNamePart{iSub} = ['*tinmarkov_Fs100Hz_nTrials' char(subSamples{iSub}) '_ICAyes.mat'];
end
%% load stuff

for iSub = 1:length(subSamples)
    for jFile=1:numP
        if ~isempty(dir([fileDir subjListP{jFile} fNamePart{iSub} ]))
            fileToRead  = dir([fileDir subjListP{jFile} fNamePart{iSub}]);
        else
            fprintf('\n shit %d ...', jFile)
            MIAp{jFile} = subjListP{jFile};
            continue
        end
        curFile = fileToRead.name;
        tmptg=load([fileDir curFile]);
        
        TG_acc_RDp(iSub,jFile,:,:) = tmptg.accTG_SND_RD;
        TG_acc_RD_MMp(iSub,jFile,:,:) = tmptg.accTG_SND_RD_MM;
        TG_acc_RD_MPp(iSub,jFile,:,:) = tmptg.accTG_SND_RD_MP;
        TG_acc_RD_ORp(iSub,jFile,:,:) = tmptg.accTG_SND_RD_OR;
        trainTimeP(iSub,jFile,:) = tmptg.time;
        jFile
        %sprintf('\nDone %s,  %d %%',subjListP{iFile}, round(100*iFile/length(subjListP)));
    end
    iSub
end
%% controls
clear tmptg* fileToRead  curFile
numC = length(subjListC);
for iSub = 1:length(subSamples)
    for jFile=1:length(subjListC)
        if ~isempty(dir([fileDir subjListC{jFile} fNamePart{iSub} ]))
            fileToRead  = dir([fileDir subjListC{jFile} fNamePart{iSub}]);
        else
            fprintf('\n shit %d ...', jFile)
            MIAc{jFile} = subjListP{jFile};
            continue
        end
        curFile = fileToRead.name;
        tmptg=load([fileDir curFile]);
        
        TG_acc_RDc(iSub,jFile,:,:) = tmptg.accTG_SND_RD;
        TG_acc_RD_MMc(iSub,jFile,:,:) = tmptg.accTG_SND_RD_MM;
        TG_acc_RD_MPc(iSub,jFile,:,:) = tmptg.accTG_SND_RD_MP;
        TG_acc_RD_ORc(iSub,jFile,:,:) = tmptg.accTG_SND_RD_OR;
        trainTimeC(iSub,jFile,:) = tmptg.time;
        jFile
        %sprintf('\nDone %s,  %d %%',subjListP{iFile}, round(100*iFile/length(subjListP)));
    end
    iSub
end
%%
%bad guys .... check and fix them ...
if exist('MIAc')
  badListC=MIAc(~cellfun('isempty',MIAc)) ;
  TG_acc_RDc = reshape(TG_acc_RDc(bsxfun(@times,any(TG_acc_RDc,2),ones(1,size(TG_acc_RDc,2)))>0),[],size(TG_acc_RDc,2),size(TG_acc_RDc,3));
  TG_acc_RD_ORc = reshape(TG_acc_RD_ORc(bsxfun(@times,any(TG_acc_RD_ORc,2),ones(1,size(TG_acc_RD_ORc,2)))>0),[],size(TG_acc_RD_ORc,2),size(TG_acc_RD_ORc,3));
  numC = size(TG_acc_RDc,1);
else end

if exist('MIAp')
  badListP=MIAp(~cellfun('isempty',MIAp)) ;
  TG_acc_RDp = reshape(TG_acc_RDp(bsxfun(@times,any(TG_acc_RDp,2),ones(1,size(TG_acc_RDp,2)))>0),[],size(TG_acc_RDp,2),size(TG_acc_RDp,3));
  TG_acc_RD_ORp = reshape(TG_acc_RD_ORp(bsxfun(@times,any(TG_acc_RD_ORp,2),ones(1,size(TG_acc_RD_ORp,2)))>0),[],size(TG_acc_RD_ORp,2),size(TG_acc_RD_ORp,3));
  numP = size(TG_acc_RDp,1);
else end

%badList = [badListC badListP]

%% Start from here with the saved data!
load /mnt/obob/staff/gdemarchi/DataAnalysis/tinnitusMarkov_GD/lisaStuff/TGallP.mat
load /mnt/obob/staff/gdemarchi/DataAnalysis/tinnitusMarkov_GD/lisaStuff/TGallC.mat

%% plot the diagonal, per trial number

tTime=linspace(-1,1,200);

for iSub = 1:length(subSamples)
    tmpdiag = zeros(200,1);
    for jFile=1:numP
        tmpdiag = tmpdiag + diag(squeeze(TG_acc_RDp(iSub,jFile,:,:)));
    end
    diagP(iSub,:)=tmpdiag/numP; %manual mean
end

for iSub = 1:length(subSamples)
    tmpdiag = zeros(200,1);
    for jFile=1:numC
        tmpdiag = tmpdiag + diag(squeeze(TG_acc_RDc(iSub,jFile,:,:)));
    end
    diagC(iSub,:)=tmpdiag/numC; %manual mean
end

figure; hold all;
plot(tTime, smoothdata(diagP), 'LineWidth',4);
xlim([-.1 .6]);
yline(.25, ':', 'LineWidth',4)
title('Training/testing RD tones per n. of trials - patients')
xlabel('time /s')
ylabel('accuracy /a.u.')
legend(subSamples)

figure; hold all;
plot(tTime, smoothdata(diagC), 'LineWidth',4);
xlim([-.1 .6]);
yline(.25, ':', 'LineWidth',4)
title('Training/testing RD tones per n. of trials - controls')
xlabel('time /s')
ylabel('accuracy /a.u.')
legend(subSamples)
%%
% smooth at the single subj level for the stat
%
clear TGsmooth*;
nInterp = 5;

for iSub = 1:length(subSamples)
    for jFile=1:numC
        TGsmooth_acc_RDc(iSub,jFile,:,:) = smooth2a(squeeze(TG_acc_RDc(iSub,jFile,:,:)),nInterp); %
        TGsmooth_acc_RD_ORc(iSub,jFile,:,:) = smooth2a(squeeze(TG_acc_RD_ORc(iSub,jFile,:,:)),nInterp); %     end
    end
end

for iSub = 1:length(subSamples)
    for jFile=1:numP
        TGsmooth_acc_RDp(iSub,jFile,:,:) = smooth2a(squeeze(TG_acc_RDp(iSub,jFile,:,:)),nInterp); %
        TGsmooth_acc_RD_ORp(iSub,jFile,:,:) = smooth2a(squeeze(TG_acc_RD_ORp(iSub,jFile,:,:)),nInterp); %     end
    end
end


%% create fake TF stuctures for later stats ...

clear tmpMat;
for iSub = 1:length(subSamples)
    for jFile=1:numC%  %ctrl
        tmpMat = TGsmooth_acc_RD_ORc(iSub,jFile,:,:) -  TGsmooth_acc_RDc(iSub,jFile,:,:); %diff OR/RD
        fTF_diff_SSc{iSub,jFile} = [];
        fTF_diff_SSc{iSub,jFile}.fsample = Fs;
        fTF_diff_SSc{iSub,jFile}.powspctrm = tmpMat;
        fTF_diff_SSc{iSub,jFile}.label = {'accuracy'};
        fTF_diff_SSc{iSub,jFile}.time = tTime;
        fTF_diff_SSc{iSub,jFile}.freq= tTime;
        fTF_diff_SSc{iSub,jFile}.dimord = 'freq_time';
    end
end

clear tmpMat;
for iSub = 1:length(subSamples)
    for jFile=1:numP %
        tmpMat = TGsmooth_acc_RD_ORp(iSub,jFile,:,:) -  TGsmooth_acc_RDp(iSub,jFile,:,:); %diff OR/RD
        fTF_diff_SSp{iSub,jFile} = [];
        fTF_diff_SSp{iSub,jFile}.fsample = Fs;
        fTF_diff_SSp{iSub,jFile}.powspctrm = tmpMat;
        fTF_diff_SSp{iSub,jFile}.label = {'accuracy'};
        fTF_diff_SSp{iSub,jFile}.time = tTime;
        fTF_diff_SSp{iSub,jFile}.freq= tTime;
        fTF_diff_SSp{iSub,jFile}.dimord = 'freq_time';
        
    end
end



%% stat
nRand = 10000;
for iSub = 1:length(subSamples)
    
    cfg = [];
    cfg.method           = 'analytic';%;analytic
    cfg.statistic        = 'ft_statfun_bayesfactor';
    cfg.numrandomization = nRand;  
    design = zeros(1,50);
    design(1,1:25) = 1;
    design(1,26:50) = 2;

    cfg.design           = design;
    cfg.ivar             = 1;    
    stat{iSub} = ft_freqstatistics(cfg,fTF_diff_SSp{iSub,:},fTF_diff_SSc{iSub,:});
    %save statDifferentSubsamples stat
end

%%

for iSub = 1:length(subSamples)
  
  cfg = [];
  cfg.frequency = [0.51 0.53];
  cfg.avgoverfreq = 'yes';

  stat_select{iSub} = ft_selectdata(cfg, stat{iSub});
  
  bf{iSub} = stat_select{iSub}.bf10;

  bf_short{iSub}=reshape(bf{iSub},1,200);

end

%%
% 100 200 500 750 1000 1500 2000 all
figure
plot(tTime,squeeze(bf{1}))
hold all
plot(tTime,squeeze(bf{2}))
plot(tTime,squeeze(bf{3}))
plot(tTime,squeeze(bf{4}))
plot(tTime,squeeze(bf{5}))
plot(tTime,squeeze(bf{6}))
plot(tTime,squeeze(bf{7}))
plot(tTime,squeeze(bf{8}))


  a=[1:200];
  bf_100 = vertcat(a, bf_short{1});
  bf_200 = vertcat(a, bf_short{2});
  bf_500 = vertcat(a, bf_short{3});
  bf_750 = vertcat(a, bf_short{4});
  bf_1000 = vertcat(a, bf_short{5});
  bf_1500 = vertcat(a, bf_short{6});
  bf_2000 = vertcat(a, bf_short{7});
  bf_all = vertcat(a, bf_short{8});

  writematrix(bf_100,'bf_100.csv')
  writematrix(bf_200,'bf_200.csv')
  writematrix(bf_500,'bf_500.csv')
  writematrix(bf_750,'bf_750.csv')
  writematrix(bf_1000,'bf_1000.csv')
  writematrix(bf_1500,'bf_1500.csv')
  writematrix(bf_2000,'bf_2000.csv')
  writematrix(bf_all,'bf_all.csv')


%%
for iSub = 1:length(subSamples)
    %
    cfg = [];
    cfg.parameter      = 'stat';
    cfg.maskparameter  = 'mask';
    cfg.maskstyle      = 'outline';
    cfg.highlight  = 'on';
    cfg.xlim = ([-.8 .0]);
    cfg.zlim = ([-3 3]);
    cfg.ylim = ([0 .66]);
    cfg.title =[' Patients vs Controls - ' subSamples{iSub} ' trials'];
    ft_singleplotTFR(cfg,stat{iSub} )
  
    %colormap(brewermap(256, '*RdYlBu'));
end

