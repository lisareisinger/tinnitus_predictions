% ADD PATHS FOR FIELTRIPTOOLBOX & MVPA & ownfuns:
clear all
clc

addpath('/mnt/obob/obob_ownft');
obob_init_ft; % Initialize obob_ownft

addpath(genpath('/mnt/obob/staff/jschubert/toolboxes/MVPA-Light'));
startup_MVPA_Light; % Initialize MVPA_Light

addpath('/mnt/obob/staff/jschubert/myfuns');
addpath('/mnt/obob/staff/jschubert/helpers');


%% load data
datadir = '/mnt/obob/staff/lreisinger/tinnitus/data/decoding_longepochs';
cond = readtable('/mnt/obob/staff/lreisinger/tinnitus/others/tinnitus.csv');
sub = cond(:, 1);
subcell = table2cell(rows2vars(sub));
all_subjects = subcell(1,[2:81]);  

load(fullfile(datadir,[all_subjects{1}, '_notinn']),'time')

diag_notinn = zeros(length(all_subjects),length(time));
diag_notinn_ord = zeros(length(all_subjects),length(time));
diag_tinn = zeros(length(all_subjects),length(time));
diag_tinn_ord = zeros(length(all_subjects),length(time));
diag_diff_tinn = zeros(length(all_subjects),length(time));
diag_diff_notinn = zeros(length(all_subjects),length(time));

tg_notinn_rnd = zeros(length(all_subjects),1,length(time),length(time));
tg_notinn_ord = zeros(length(all_subjects),1,length(time),length(time));
tg_tinn_rnd = zeros(length(all_subjects),1,length(time),length(time));
tg_tinn_ord = zeros(length(all_subjects),1,length(time),length(time));

diff_tinn = zeros(length(all_subjects),1,length(time), length(time));
diff_notinn = zeros(length(all_subjects),1,length(time), length(time));

for subi = 1:length(all_subjects)
  subject_id = all_subjects{subi};
  cfg = [];
  result = cond(strcmp(cond.subject_id, subject_id), 2);
  if result{1,1} == 0
  condition = 'notinn'; 
    load(fullfile(datadir,[subject_id,'_notinn.mat']),'acc_rand')
    load(fullfile(datadir,[subject_id,'_notinn.mat']),'acc_cross')
    diag_notinn(subi,:) = diag(acc_rand');
    diag_notinn_ord(subi,:) = diag(acc_cross');
    tg_notinn_rnd(subi,1,:,:) = acc_rand;
    tg_notinn_ord(subi,1,:,:) = acc_cross;
    diff_notinn(subi,1,:,:) = acc_cross - acc_rand;
    diag_diff_notinn(subi,:) = diag(acc_cross') - diag(acc_rand');
    
  else
  condition = 'tinn'; 
    load(fullfile(datadir,[subject_id,'_tinn.mat']),'acc_rand')
    load(fullfile(datadir,[subject_id,'_tinn.mat']),'acc_cross')
    diag_tinn(subi,:) = diag(acc_rand');
    diag_tinn_ord(subi,:) = diag(acc_cross');
    tg_tinn_rnd(subi,1,:,:) = acc_rand;
    tg_tinn_ord(subi,1,:,:) = acc_cross;
    diff_tinn(subi,1,:,:) = acc_cross - acc_rand;
    diag_diff_tinn(subi,:) = diag(acc_cross') - diag(acc_rand');
  end 
end

diag_tinn(all(diag_tinn==0,2),:) = [];
diag_tinn_ord(all(diag_tinn_ord==0,2),:) = [];
diag_diff_tinn(all(diag_diff_tinn==0,2),:) = [];
diag_diff_notinn(all(diag_diff_notinn==0,2),:) = [];
diag_notinn(all(diag_notinn==0,2),:) = [];
diag_notinn_ord(all(diag_notinn_ord==0,2),:) = [];


idx_zero = find(tg_notinn_ord(:,1,1,1) == 0);
tg_notinn_ord(idx_zero,:,:,:) = [];
tg_notinn_rnd(idx_zero,:,:,:) = [];

idx_zero = find(tg_tinn_ord(:,1,1,1) == 0);
tg_tinn_ord(idx_zero,:,:,:) = [];
tg_tinn_rnd(idx_zero,:,:,:) = [];

idx_zero = find(diff_tinn(:,1,1,1) == 0);
diff_tinn(idx_zero,:,:,:) = [];

idx_zero = find(diff_notinn(:,1,1,1) == 0);
diff_notinn(idx_zero,:,:,:) = [];

%% smoothing

nInterp = 5;
numP = length(all_subjects)/2;
for iFile=1:numP
  tg_tinn_ords(iFile,1,:,:) = squeeze(smooth2a(squeeze(tg_tinn_ord(iFile,1,:,:)),nInterp)); %
  tg_tinn_rnds(iFile,1,:,:) = squeeze(smooth2a(squeeze(tg_tinn_rnd(iFile,1,:,:)),nInterp)); %
  tg_notinn_ords(iFile,1,:,:) = squeeze(smooth2a(squeeze(tg_notinn_ord(iFile,1,:,:)),nInterp)); %
  tg_notinn_rnds(iFile,1,:,:) = squeeze(smooth2a(squeeze(tg_notinn_rnd(iFile,1,:,:)),nInterp)); % 
  
  diff_tinns(iFile,1,:,:) = squeeze(smooth2a(squeeze(diff_tinn(iFile,1,:,:)),nInterp)); %
  diff_notinns(iFile,1,:,:) = squeeze(smooth2a(squeeze(diff_notinn(iFile,1,:,:)),nInterp)); %

end


%% stats of diag

lower_notinn = mean(diag_notinn,1) - (1.96.*(std(diag_notinn)./sqrt(size(diag_notinn,1))));
lower_tinn = mean(diag_tinn,1) - (1.96.*(std(diag_tinn)./sqrt(size(diag_tinn,1))));

mask_notinn = lower_notinn > 0.25;
mask_tinn = lower_tinn > 0.25;

%% plot diag
cmap = [3, 142, 130; 44, 48, 151]./255;

figure
cfg = [];
cfg.alpha = 'ci';
cfg.mask = [mask_notinn*0.21;  mask_tinn*0.20;]; % values give y-axis
cfg.xlim = [-0.2 0.5];
cfg.legend = {'No Tinnitus', 'Tinnitus'};
cfg.xlabel = 'Time (s)';
cfg.ylabel = 'Decoding Accurracy';
cfg.yline= 0.25;
cfg.xline= 0;
cfg.title = 'Feature Decoding Random';
js_plot_shadedline(cfg, time, diag_notinn, diag_tinn,'Color',cmap)

figure
cfg = [];
cfg.alpha = 'ci';
cfg.xlim = [-0.2 0.5];
cfg.legend = {'ordered', 'random'};
cfg.xlabel = 'time';
cfg.ylabel = 'decoding accurracy';
cfg.yline= 0.25;
cfg.xline= 0;
cfg.title = 'Feature Decoding: Notinn';
js_plot_shadedline(cfg, time, diag_notinn_ord, diag_notinn,'Color',cmap, 'Linestyle',{'-','--'})

%

figure
cfg = [];
cfg.alpha = 'ci';
cfg.xlim = [-0.2 0.5];
cfg.legend = {'ordered', 'random'};
cfg.xlabel = 'time';
cfg.ylabel = 'decoding accurracy';
cfg.yline= 0.25;
cfg.xline= 0;
cfg.title = 'Feature Decoding: Tinn';
js_plot_shadedline(cfg, time, diag_tinn_ord, diag_tinn,'Color',cmap, 'Linestyle',{'-','--'})


%% fake fieldtrip struct
dat_notinn_rnd = [];
dat_notinn_rnd.fsample = 100;
dat_notinn_rnd.label = {'accuracy'};
dat_notinn_rnd.time = time;
dat_notinn_rnd.freq= time;
dat_notinn_rnd.dimord = 'subj_chan_freq_time';
dat_notinn_rnd.powspctrm = tg_notinn_rnds;

dat_notinn_ord = dat_notinn_rnd;
dat_notinn_ord.powspctrm = tg_notinn_ords;

dat_tinn_rnd = dat_notinn_rnd;
dat_tinn_rnd.powspctrm = tg_tinn_rnds;

dat_tinn_ord = dat_notinn_rnd;
dat_tinn_ord.powspctrm = tg_tinn_ords;

%% stats of tempgen
clear stat
cfg = [];
cfg.method           = 'montecarlo';
cfg.statistic        = 'ft_statfun_depsamplesT';
cfg.correctm         = 'cluster';
cfg.clusteralpha     = 0.05;
cfg.clusterstatistic = 'maxsum'; %default = 'maxsum'
cfg.tail             = 0; %two-sided
cfg.clustertail      = 0; %two-sided
cfg.alpha            = 0.05;
cfg.numrandomization = 1000;
cfg.latency = [-0.5, 0]; % testwin
cfg.frequency = [0,0.8]; % trainwin

% 
% prepare design
n = length(tg_notinn_ord(:,1,1,1));
design = zeros(2,n*2);
design(1,1:n) = 1:n;
design(1,n+1:end) = 1:n;
design(2,1:n) = 1;
design(2,n+1:end) =2;

cfg.design   = design;
cfg.uvar     = 1;
cfg.ivar     = 2;

stat_notinn = ft_freqstatistics(cfg, dat_notinn_ord, dat_notinn_rnd);
stat_tinn = ft_freqstatistics(cfg, dat_tinn_ord, dat_tinn_rnd);

%%

figure
cfg_plot= [];
cfg_plot.y   = stat_notinn.freq;
cfg_plot.ylabel = 'training time (s)';
cfg_plot.x   = stat_notinn.time;
cfg_plot.xlabel = 'testing time (s)';
cfg_plot.climzero = 0;
cfg_plot.clim = [-3 3];
cfg_plot.mask = squeeze(stat_notinn.mask);
cfg_plot.title = 'notinn: ordered > random';
call_js_private('mv_plot_2D',cfg_plot, smooth2a(squeeze(stat_notinn.stat),2));

figure
cfg_plot.mask = squeeze(stat_tinn.mask);
cfg_plot.title = 'tinn: ordered > random';
call_js_private('mv_plot_2D',cfg_plot, smooth2a(squeeze(stat_tinn.stat),2));


%% compare tinn and notinn

% fake fieldtrip struct
dat_diff_notinn = [];
dat_diff_notinn.fsample = 100;
dat_diff_notinn.label = {'accuracy'};
dat_diff_notinn.time = time;
dat_diff_notinn.freq= time;
dat_diff_notinn.dimord = 'subj_chan_freq_time';
dat_diff_notinn.powspctrm = diff_notinns;                   %diff_notinn; oder tg_notinn_ord;

dat_diff_tinn = [];
dat_diff_tinn = dat_diff_notinn;
dat_diff_tinn.powspctrm = diff_tinns;                       %diff_tinn; oder tg_tinn_ord;


% figure; 
% imagesc(smooth2a((squeeze(mean(dat_diff_tinn.powspctrm) - mean(dat_diff_notinn.powspctrm))),2));
% hold on;
% line([43,43], [0,91], 'Color','b');
% line([0,91], [43,43], 'Color','b');
% hold off;
% set(gca,'YDir','normal')

%% select specific training time

cfg = [];
cfg.frequency = [.47, .57];
cfg.avgoverfreq = 'yes';
dat_diff_tinn_select = ft_selectdata(cfg,dat_diff_tinn);
dat_diff_notinn_select = ft_selectdata(cfg,dat_diff_notinn);


% plot(dat_diff_notinn_select.time,mean(squeeze(dat_diff_notinn_select.powspctrm)))
cmap = [3, 142, 130; 44, 48, 151]./255;

figure
cfg = [];
%cfg.alpha = 'ci';
%cfg.xlim = [-0.2 0.5];
cfg.legend = {'notinn', 'tinn'};
cfg.xlabel = 'time';
cfg.xline= 0;
cfg.ylabel = 'decoding accurracy';
js_plot_shadedline(cfg, time, squeeze(dat_diff_notinn_select.powspctrm),squeeze(dat_diff_tinn_select.powspctrm),'Color',cmap, 'Linestyle',{'-','-'})

%% highest value difference between groups

dat_diff_tinn_select.powspctrm = squeeze(dat_diff_tinn_select.powspctrm);
dat_diff_notinn_select.powspctrm = squeeze(dat_diff_notinn_select.powspctrm);

tinn_tp = dat_diff_tinn_select.powspctrm(:,15);
notinn_tp = dat_diff_notinn_select.powspctrm(:,15);

a=111;
tinn_tp = vertcat(a,tinn_tp);
notinn_tp = vertcat(a,notinn_tp);

% writematrix(tinn_tp,'tinn_window.csv')
% writematrix(notinn_tp,'notinn_window.csv')

%% diff selection

% stats of tempgen
clear stat
cfg = [];
cfg.method           = 'montecarlo';
cfg.statistic        = 'ft_statfun_indepsamplesT';
cfg.correctm         = 'analytic';
cfg.clusteralpha     = 0.05;
cfg.numrandomization = 1000;

% prepare design
n = length(diff_notinn(:,1,1,1));
design = zeros(1,n*2);
design(1,1:n) = 1;
design(1,n+1:end) = 2;

cfg.design   = design;
cfg.ivar     = 1;

stat_diff_select = ft_freqstatistics(cfg, dat_diff_tinn_select, dat_diff_notinn_select);

plot(stat_diff_select.time,squeeze(stat_diff_select.prob))

%% diff stat

% stats of tempgen
clear stat
cfg = [];
cfg.method           = 'montecarlo';
cfg.statistic        = 'ft_statfun_indepsamplesT';
cfg.correctm         = 'cluster';
cfg.clusteralpha     = 0.05;
cfg.clusterstatistic = 'maxsum'; %default = 'maxsum'
cfg.avgoverfreq      = 'yes';
cfg.tail             = 1;
cfg.clustertail      = 1; 
cfg.alpha            = 0.05;
cfg.numrandomization = 1000;
cfg.latency = [-0.4, 0]; % testwin   [-0.4, 0]
cfg.frequency = [0.47, 0.57]; % trainwin   [0.47,0.57]


% prepare design
n = length(diff_notinn(:,1,1,1));
design = zeros(1,n*2);
design(1,1:n) = 1;
design(1,n+1:end) = 2;

cfg.design   = design;
cfg.ivar     = 1;

stat_diff = ft_freqstatistics(cfg, dat_diff_tinn, dat_diff_notinn);


%% diff figure

figure
cfg_plot= [];
cfg_plot.y   = stat_diff.freq;
cfg_plot.ylabel = 'training time (s)';
cfg_plot.x   = stat_diff.time;
cfg_plot.xlabel = 'testing time (s)';
cfg_plot.climzero = 0;
cfg_plot.clim = [-3 3];
cfg_plot.mask = squeeze(stat_diff.mask);
cfg_plot.title = 'Tinnitus > No Tinnitus';
call_js_private('mv_plot_2D',cfg_plot, smooth2a(squeeze(stat_diff.stat),2));


%% diag stat (random frequency decoding)

avg_diag_tinn = mean(diag_tinn,1);
avg_diag_notinn = mean(diag_notinn,1);
[h,p,ci,stats] = ttest2(avg_diag_tinn,avg_diag_notinn)

test = zeros(40,1);
[h,p,ci,stats] = ttest2(tinn_tp,test)
[h,p,ci,stats] = ttest2(notinn_tp,test)

% avg_diag_tinn_ord = mean(diag_tinn_ord(:,1:52),2);


