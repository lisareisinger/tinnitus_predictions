% ADD PATHS FOR FIELTRIPTOOLBOX & MVPA & ownfuns:
clear all
clc

addpath('/mnt/obob/obob_ownft');
obob_init_ft; % Initialize obob_ownft

addpath(genpath('/mnt/obob/staff/jschubert/toolboxes/MVPA-Light'));
startup_MVPA_Light; % Initialize MVPA_Light

addpath('/mnt/obob/staff/jschubert/myfuns');
addpath('/mnt/obob/staff/jschubert/helpers');

addpath /mnt/obob/staff/gdemarchi/mattools/;
addpath /mnt/obob/staff/gdemarchi/git/export_fig/;
addpath /mnt/obob/staff/gdemarchi/DataAnalysis/tinnitusMarkov/decoding/functions/

%% load sbg hearing data

datadir = '/mnt/obob/staff/lreisinger/tinnitus/data/decoding_longepochs';
cond = readtable('/mnt/obob/staff/lreisinger/tinnitus/others/tinnitus.csv');
sub = cond(:, 1);
subcell = table2cell(rows2vars(sub));
all_subjects = subcell(1,[2:81]);  
%subjectlist = all_subjects; manually select relevant participants
%all_subjects = subjectlist';
%bad_subjects = {'19530712mrri', '19531220enhr', '19591018mrrt', '19910612crke'};
%all_subjects = setdiff(all_subjects, bad_subjects);

load(fullfile(datadir,[all_subjects{1}, '_notinn']),'time')

diag_notinn = zeros(length(all_subjects),length(time));
diag_notinn_ord = zeros(length(all_subjects),length(time));
diag_tinn = zeros(length(all_subjects),length(time));
diag_tinn_ord = zeros(length(all_subjects),length(time));
diag_diff_tinn = zeros(length(all_subjects),length(time));
diag_diff_notinn = zeros(length(all_subjects),length(time));

tg_notinn_rnd = zeros(length(all_subjects),1,length(time),length(time));
tg_notinn_ord = zeros(length(all_subjects),1,length(time),length(time));
tg_tinn_rnd = zeros(length(all_subjects),1,length(time),length(time));
tg_tinn_ord = zeros(length(all_subjects),1,length(time),length(time));

diff_tinn = zeros(length(all_subjects),length(time), length(time));
diff_notinn = zeros(length(all_subjects),length(time), length(time));

for subi = 1:length(all_subjects)
  subject_id = all_subjects{subi};
  cfg = [];
  result = cond(strcmp(cond.subject_id, subject_id), 2);
  if result{1,1} == 0
  condition = 'notinn'; 
    load(fullfile(datadir,[subject_id,'_notinn.mat']),'acc_rand')
    load(fullfile(datadir,[subject_id,'_notinn.mat']),'acc_cross')
    diag_notinn(subi,:) = diag(acc_rand');
    diag_notinn_ord(subi,:) = diag(acc_cross');
    tg_notinn_rnd(subi,1,:,:) = acc_rand;
    tg_notinn_ord(subi,1,:,:) = acc_cross;
    diff_notinn(subi,:,:) = acc_cross - acc_rand;
    diag_diff_notinn(subi,:) = diag(acc_cross') - diag(acc_rand');
    
  else
  condition = 'tinn'; 
    load(fullfile(datadir,[subject_id,'_tinn.mat']),'acc_rand')
    load(fullfile(datadir,[subject_id,'_tinn.mat']),'acc_cross')
    diag_tinn(subi,:) = diag(acc_rand');
    diag_tinn_ord(subi,:) = diag(acc_cross');
    tg_tinn_rnd(subi,1,:,:) = acc_rand;
    tg_tinn_ord(subi,1,:,:) = acc_cross;
    diff_tinn(subi,:,:) = acc_cross - acc_rand;
    diag_diff_tinn(subi,:) = diag(acc_cross') - diag(acc_rand');
  end 
end

diag_tinn(all(diag_tinn==0,2),:) = [];
diag_tinn_ord(all(diag_tinn_ord==0,2),:) = [];
diag_diff_tinn(all(diag_diff_tinn==0,2),:) = [];
diag_diff_notinn(all(diag_diff_notinn==0,2),:) = [];
diag_notinn(all(diag_notinn==0,2),:) = [];
diag_notinn_ord(all(diag_notinn_ord==0,2),:) = [];


idx_zero = find(tg_notinn_ord(:,1,1,1) == 0);
tg_notinn_ord(idx_zero,:,:,:) = [];
tg_notinn_rnd(idx_zero,:,:,:) = [];

idx_zero = find(tg_tinn_ord(:,1,1,1) == 0);
tg_tinn_ord(idx_zero,:,:,:) = [];
tg_tinn_rnd(idx_zero,:,:,:) = [];

idx_zero = find(diff_tinn(:,1,1) == 0);
diff_tinn(idx_zero,:,:) = [];

idx_zero = find(diff_notinn(:,1,1) == 0);
diff_notinn(idx_zero,:,:) = [];

% smoothing

nInterp = 5;
numP = length(all_subjects)/2;
for iFile=1:numP
  tg_tinn_ords(iFile,1,:,:) = squeeze(smooth2a(squeeze(tg_tinn_ord(iFile,1,:,:)),nInterp)); %
  tg_tinn_rnds(iFile,1,:,:) = squeeze(smooth2a(squeeze(tg_tinn_rnd(iFile,1,:,:)),nInterp)); %
  tg_notinn_ords(iFile,1,:,:) = squeeze(smooth2a(squeeze(tg_notinn_ord(iFile,1,:,:)),nInterp)); %
  tg_notinn_rnds(iFile,1,:,:) = squeeze(smooth2a(squeeze(tg_notinn_rnd(iFile,1,:,:)),nInterp)); % 
  
  diff_tinns(iFile,:,:) = squeeze(smooth2a(squeeze(diff_tinn(iFile,:,:)),nInterp)); %
  diff_notinns(iFile,:,:) = squeeze(smooth2a(squeeze(diff_notinn(iFile,:,:)),nInterp)); %
end

lower_notinn = mean(diag_notinn,1) - (1.96.*(std(diag_notinn)./sqrt(size(diag_notinn,1))));
lower_tinn = mean(diag_tinn,1) - (1.96.*(std(diag_tinn)./sqrt(size(diag_tinn,1))));

mask_notinn = lower_notinn > 0.25;
mask_tinn = lower_tinn > 0.25;

%% load old data

fileDir = (['/mnt/obob/staff/gdemarchi/data/tinnitusMarkov/decoding/TG_trSNDteSND/']);

% TINNITUS PATIENTS

subjListP =  {
'181108ATLN', %	Tinnitus
'171128BASH', % Tinnitus
'180516CRGL', %	Tinnitus
'180514PTBU', %	Tinnitus
'171215SBOE', %	Tinnitus
'180810GBSR', %	Tinnitus
'181114EILC', %	Tinnitus
'180516GASA', %	Tinnitus
'190304EMLP', %	Tinnitus
'181109CRBU', %	Tinnitus
'170704FAZC', %	Tinnitus
'180626PISH', %	Tinnitus
'170418cris', %	Tinnitus
'180424ANME', %	Tinnitus
'170928AOHM', %	Tinnitus
'180718RSAT', %	Tinnitus
'171020SSSH', %	Tinnitus
'180718ANFI', %	Tinnitus
'180306ZHCU', %	Tinnitus
'180417CRAZ', %	Tinnitus
'190116CASI', %	Tinnitus
'171020CRRI', %	Tinnitus
'170505WLTI', %	Tinnitus
% '180619MRLI', %	Tinnitus % not in group c.
'170428MRBR', %	Tinnitus
'180514MRTE', %	Tinnitus
% '171128BRKE', %	Tinnitus % not in group c.
% '180525MRPA', %	Tinnitus % not in group c.
% '180626EIKN', %	Tinnitus % not in group c. 
% '171020EIGA', %	Tinnitus % not in group c.
% '170704HDEM', %	Tinnitus % not in group c.
% %'190325HDHB', %	Tinnitus % not in group c.
%'170609JHSH', %	Tinnitus % not in group c.
%'180619ANGU', %	Tinnitus % not in group c.
 };

%%
%clear fake* TG_acc*  data* comp* tmptg*   fileToRead  curFile
Fs = 100;
numP = length(subjListP);

clear TG_acc* gaTF*
%
fNamePart = '*timegen_trOnRdSND_teSND.mat';
for iFile=1:numP
  if ~isempty(dir([fileDir subjListP{iFile} fNamePart ]))
    fileToRead  = dir([fileDir subjListP{iFile} fNamePart]);
  else
    fprintf('\n shit %d ...', iFile)
    MIAp{iFile} = subjListP{iFile};
    continue
  end
  curFile = fileToRead.name;
  tmptg=load([fileDir curFile]);
  
  TG_acc_RDp(iFile,1,:,:) = tmptg.accTG_SND_RD;
  TG_acc_RD_ORp(iFile,1,:,:) = tmptg.accTG_SND_RD_OR;
  diag_rand_p(iFile,:) = diag(tmptg.accTG_SND_RD');
  diag_cross_p(iFile,:) = diag(tmptg.accTG_SND_RD_OR');
  diag_diff_p(iFile,:) = diag(tmptg.accTG_SND_RD_OR') - diag(tmptg.accTG_SND_RD');
  diff_all_p(iFile,:,:) = tmptg.accTG_SND_RD_OR - tmptg.accTG_SND_RD;
  trainTimeP{iFile} = tmptg.time;
  iFile
  %sprintf('\nDone %s,  %d %%',subjListP{iFile}, round(100*iFile/length(subjListP)));
end
tTime = tmptg.time;


% bad guys .... check and fix them ...
if exist('MIAp')
  badList=MIAp(~cellfun('isempty',MIAp)) ;
else end

diag_rand_p_short = diag_rand_p(:,53:183);
diag_cross_p_short = diag_cross_p(:,53:183);
diag_diff_p_short = diag_diff_p(:,53:183);

%% smooth at the single subj level for the stat

clear TG_acc_smooth*;
nInterp = 5;
for iFile=1:numP
  TG_acc_smoothRdSNDOrSNDp(iFile,1,:,:) = smooth2a(squeeze(TG_acc_RD_ORp(iFile,1,:,:)),nInterp); %
  TG_acc_smoothRdSNDRdSNDp(iFile,1,:,:) = smooth2a(squeeze(TG_acc_RDp(iFile,1,:,:)),nInterp); % 
  diff_all_ps(iFile,:,:) = smooth2a(squeeze(diff_all_p(iFile,:,:)),nInterp); 
end


%% the same for  %% CONTROLS, 25 as the Tinnitus target groups
subjListC =  {
'170616SBPE', %	Control
'170624BTKC', %	Control
'170624EEHB', %	Control
'170624GNTA', %	Control
'170511MNSU', %	Control
'170517KTAD', %	Control
'170511CRBC', %	Control
'170718GDZN', %	Control
'170718SLBR', %	Control
'170906PNRK', %	Control
'190119KTML', %	Control
'181109JSHP', %	Control
'190125MNPR', %	Control
'190112MRSH', %	Control
'190124JSJB', %	Control
'180607RSAS', %	Control
'190107MRKB', %	Control
'170511GBHL', %	Control
'170906KRHR', %	Control
'181218PIKN', %	Control
'181017MRPC', %	Control
'190112MRHC', %	Control
'190119CRLN', %	Control
'190129MRBN', %	Control
'180626ANMY', %	Control
  };

%%
clear tmptg* fileToRead  curFile

Fs = 100;
numC = length(subjListC);

fNamePart = '*timegen_trOnRdSND_teSND.mat';

for iFile=1:length(subjListC)
  if ~isempty(dir([fileDir subjListC{iFile} fNamePart ]))
    fileToRead  = dir([fileDir subjListC{iFile} fNamePart]);
  else
    fprintf('\n shit %d ...', iFile)
    MIAc{iFile} = subjListC{iFile};
    continue
  end
  curFile = fileToRead.name;
  tmptg=load([fileDir curFile]);
  
  TG_acc_RDc(iFile,1,:,:) = tmptg.accTG_SND_RD;
  TG_acc_RD_ORc(iFile,1,:,:) = tmptg.accTG_SND_RD_OR;
   diag_rand_c(iFile,:) = diag(tmptg.accTG_SND_RD');
  diag_cross_c(iFile,:) = diag(tmptg.accTG_SND_RD_OR');
  diag_diff_c(iFile,:) = diag(tmptg.accTG_SND_RD_OR') - diag(tmptg.accTG_SND_RD');
  trainTimeC{iFile} = tmptg.time;
  diff_all_c(iFile,:,:) = tmptg.accTG_SND_RD_OR - tmptg.accTG_SND_RD;
  iFile
  %sprintf('\nDone %s,  %d %%',subjListP{iFile}, round(100*iFile/length(subjListP)));
end
tTime = tmptg.time;

diag_rand_c_short = diag_rand_c(:,53:183);
diag_cross_c_short = diag_cross_c(:,53:183);
diag_diff_c_short = diag_diff_c(:,53:183);

%% smooth at the single subj level for the stat

clear TG_acc_smooth*;
nInterp = 5;
for iFile=1:numC
  TG_acc_smoothRdSNDOrSNDc(iFile,1,:,:) = smooth2a(squeeze(TG_acc_RD_ORc(iFile,1,:,:)),nInterp); %
  TG_acc_smoothRdSNDRdSNDc(iFile,1,:,:) = smooth2a(squeeze(TG_acc_RDc(iFile,1,:,:)),nInterp); % 
  diff_all_cs(iFile,:,:) = smooth2a(squeeze(diff_all_c(iFile,:,:)),nInterp); 
end

%% put together diag_notinn and diag_tinn
% put together diag_rand_p and diag_rand_c

a=[1:131];

diag_lisa = vertcat(diag_notinn,diag_tinn);
diag_gp = vertcat(diag_rand_p_short,diag_rand_c_short);

lisa = vertcat(a,diag_diff_notinn);
diag_diff_lisa = vertcat(lisa, diag_diff_tinn);
gp = vertcat(a,diag_diff_c_short);
diag_diff_gp = vertcat(gp, diag_diff_p_short);
%time = vertcat(a,time);

lisa_rand = vertcat(a,diag_notinn);
lisa_rand = vertcat(lisa_rand,diag_tinn);

lisa_ord = vertcat(a,diag_notinn_ord);
lisa_ord = vertcat(lisa_ord,diag_tinn_ord);

gp_rand = vertcat(a, diag_rand_c_short);
gp_rand = vertcat(gp_rand,diag_rand_p_short);

gp_ord = vertcat(a, diag_cross_c_short);
gp_ord = vertcat(gp_ord,diag_cross_p_short);

% writematrix(diag_diff_lisa,'diag_diff_lisa.csv') 
% writematrix(diag_diff_gp,'diag_diff_gp.csv')
% 
% writematrix(lisa_rand,'lisa_rand.csv')
% writematrix(lisa_ord,'lisa_ord.csv')
% 
% writematrix(gp_rand,'gp_rand.csv')
% writematrix(gp_ord,'gp_ord.csv')

%%

cmap = [3, 142, 130; 44, 48, 151]./255;

figure
cfg = [];
cfg.alpha = 'ci';
cfg.mask = [mask_notinn*0.21;  mask_tinn*0.20;]; % values give y-axis
cfg.xlim = [-0.2 0.5];
cfg.legend = {'GP', 'Lisa'};
cfg.xlabel = 'Time (s)';
cfg.ylabel = 'Decoding Accurracy';
cfg.yline= 0.25;
cfg.xline= 0;
cfg.title = 'Feature Decoding Random';
js_plot_shadedline(cfg, time, diag_gp, diag_lisa,'Color',cmap)

avg_diag_gp = mean(diag_gp,1);
avg_diag_lisa = mean(diag_lisa,1);
[h,p,ci,stats] = ttest2(avg_diag_lisa,avg_diag_gp)

%% bayesfactor

% fake fieldtrip struct
dat_diff_lisa_notinn = [];
dat_diff_lisa_notinn.fsample = 100;
dat_diff_lisa_notinn.label = {'accuracy'};
dat_diff_lisa_notinn.time = time;
dat_diff_lisa_notinn.freq= time;
dat_diff_lisa_notinn.dimord = 'subj_freq_time';
dat_diff_lisa_notinn.powspctrm = diff_notinns;    

dat_diff_lisa_tinn = [];
dat_diff_lisa_tinn.fsample = 100;
dat_diff_lisa_tinn.label = {'accuracy'};
dat_diff_lisa_tinn.time = time;
dat_diff_lisa_tinn.freq= time;
dat_diff_lisa_tinn.dimord = 'subj_freq_time';
dat_diff_lisa_tinn.powspctrm = diff_tinns;   

diff_gp_p = diff_all_ps(:,53:183,53:183);
diff_gp_c = diff_all_cs(:,53:183,53:183);

dat_diff_gp_p = [];
dat_diff_gp_p.fsample = 100;
dat_diff_gp_p.label = {'accuracy'};
dat_diff_gp_p.time = time;
dat_diff_gp_p.freq= time;
dat_diff_gp_p.dimord = 'subj_freq_time';
dat_diff_gp_p.powspctrm = diff_gp_p; 

dat_diff_gp_c = [];
dat_diff_gp_c.fsample = 100;
dat_diff_gp_c.label = {'accuracy'};
dat_diff_gp_c.time = time;
dat_diff_gp_c.freq= time;
dat_diff_gp_c.dimord = 'subj_freq_time';
dat_diff_gp_c.powspctrm = diff_gp_c; 
   

%% 
clear stat
cfg = [];
cfg.method           = 'analytic';
cfg.statistic        = 'ft_statfun_bayesfactor';
%cfg.correctm         = 'analytic';
cfg.numrandomization = 10000;

% prepare design
design = zeros(1,50);
design(1,1:25) = 1;
design(1,26:50) = 2;

cfg.design   = design;
cfg.ivar     = 1;

stat_diff_gp = ft_freqstatistics(cfg, dat_diff_gp_c, dat_diff_gp_p);

% prepare design lisa
design = zeros(1,80);
design(1,1:40) = 1;
design(1,41:80) = 2;

cfg.design   = design;
cfg.ivar     = 1;

stat_diff_lisa = ft_freqstatistics(cfg, dat_diff_lisa_notinn, dat_diff_lisa_tinn);

%% select stats
cfg = [];
cfg.frequency = [0.51 0.53];
cfg.avgoverfreq = 'yes';

stats_selected_gp = ft_selectdata(cfg, stat_diff_gp);
stats_selected_lisa = ft_selectdata(cfg,stat_diff_lisa);
plot(stats_selected_gp.time, squeeze(stats_selected_gp.bf10));
plot(stats_selected_lisa.time, squeeze(stats_selected_lisa.bf10));

a=[1:131];
gp_bf = vertcat(a, stats_selected_gp.bf10);
lisa_bf = vertcat(a,stats_selected_lisa.bf10);
% writematrix(lisa_bf,'bf10_lisa.csv')
% writematrix(gp_bf,'bf10_gp.csv')

%% select relevant time for further analysis
cfg = [];
cfg.frequency = [.47, .57];
cfg.avgoverfreq = 'yes';
dat_diff_lisa_tinn_select = ft_selectdata(cfg,dat_diff_lisa_tinn);
dat_diff_lisa_notinn_select = ft_selectdata(cfg,dat_diff_lisa_notinn);
dat_diff_gp_c_select = ft_selectdata(cfg,dat_diff_gp_c);
dat_diff_gp_p_select = ft_selectdata(cfg,dat_diff_gp_p);

a=[1:131];
df_lisa_notinn_select_hl = vertcat(a,squeeze(dat_diff_lisa_notinn_select.powspctrm));
df_lisa_tinn_select_hl = vertcat(a,squeeze(dat_diff_lisa_tinn_select.powspctrm));
% writematrix(df_lisa_notinn_select_hl,'select_hl_notinn.csv')
% writematrix(df_lisa_tinn_select_hl,'select_hl_tinn.csv')