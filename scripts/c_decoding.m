function c_decoding(subject_id, datadir, outdir)

% ADD PATHS FOR FIELTRIPTOOLBOX & MVPA & ownfuns:
addpath('/mnt/obob/obob_ownft');
obob_init_ft; % Initialize obob_ownft

addpath('/mnt/obob/staff/jschubert/myfuns'); % must be set after obob_init_ft
addpath('/mnt/obob/staff/jschubert/helpers');
addpath('/mnt/obob/staff/lreisinger/tinnitus');

addpath(genpath('/mnt/obob/staff/jschubert/toolboxes/MVPA-Light'));
startup_MVPA_Light; % Initialize MVPA_Light
cond = readtable('/mnt/obob/staff/lreisinger/tinnitus/others/tinnitus.csv');

%% load data

cfg = [];

result = cond(strcmp(cond.subject_id, subject_id), 2);
if result{1,1} == 0
  condition = 'notinn';
  data_notinn = struct2cell(load(fullfile(datadir,[subject_id, '_notinn.mat']), 'data'));
  data_notinn = data_notinn{1,1};
%   data_notinn{1,1}.trial = data_notinn{1,1}.trial(1:1498)
%   data_notinn{1,1}.time = data_notinn{1,1}.time(1:1498)
%   data_notinn{1,1}.trialinfo = data_notinn{1,1}.trialinfo(1:1498,:)
  all_data = ft_appenddata(cfg, data_notinn{:});
else
  condition = 'tinn';
  data_tinn = struct2cell(load(fullfile(datadir,[subject_id, '_tinn.mat']), 'data'));
  data_tinn = data_tinn{1,1};
  all_data = ft_appenddata(cfg, data_tinn{:});
end

% all_data.trial = all_data.trial(2:end)
% all_data.time = all_data.time(2:end)
% all_data.trialinfo = all_data.trialinfo(2:end,:)

%% prepare for decoding


% add trialnr info! (keep in mind that we have separate blocks though)
all_data.trialinfo(:,3) = 1:length(all_data.trialinfo);

% reshape trials
all_data = functions.js_fuckufieldtrip(all_data); %reshapes trials into a 3D-matrix
all_data.trialinfo(:,4) = functions.get_transitions(all_data.trialinfo(:,2));
time = all_data.time;

% get ordered/random trials
ord_trials = all_data.trial(all_data.trialinfo(:,1) == 1,:,:);
ord_trialinfo = all_data.trialinfo(all_data.trialinfo(:,1) == 1,:);

rand_trials = all_data.trial(all_data.trialinfo(:,1) == 2,:,:);
rand_trialinfo = all_data.trialinfo(all_data.trialinfo(:,1) == 2,:);


%% do decoding

acc = [];
cfg = [];
cfg.classifier = 'multiclass_lda';
cfg.metric = 'acc';
cfg.preprocess = {'undersample' 'zscore'};  

% pparam = mv_get_preprocess_param('zscore');
% z_rand = mv_preprocess_zscore(pparam, rand_trials);
% z_ord = mv_preprocess_zscore(pparam, ord_trials);

[acc_rand, result_acc_rand] = functions.mv_classify_timextime(cfg, rand_trials, rand_trialinfo(:,2));
%[acc_ord, result_acc_ord] = functions.mv_classify_timextime(cfg, ord_trials, ord_trialinfo(:,2));
[acc_cross, result_acc_cross] = functions.mv_classify_timextime(cfg, rand_trials, rand_trialinfo(:,2), ord_trials, ord_trialinfo(:,2)); % second dataset will be used as test set

%% build the weights structure

weights = [];
weights.cf_cross        = result_acc_cross.cf;
weights.cf_rand         = result_acc_rand.cf;
weights.data_rand       = rand_trials;
weights.data_ord        = ord_trials;
weights.time            = time;
weights.trialinfo_rand  = rand_trialinfo(:,2);
weights.trialinfo_ord   = ord_trialinfo(:,2);
weights.grad            = all_data.grad;
weights.label           = all_data.label;

%% save

save(fullfile(outdir, [subject_id, '_', condition, '.mat']),'acc_rand', 'acc_cross', 'weights','-v7.3');


end % of classdef