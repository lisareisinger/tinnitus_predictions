%% RUNNING TONES DECODING ON CLUSTER (all_subjects):
% add paths & init packages:
clc
clear all
addpath('/mnt/obob/obob_ownft');

cfg = [];
cfg.package.plus_slurm = true;
obob_init_ft(cfg); % Initialize obob_ownft

addpath('/mnt/obob/staff/jschubert/myfuns'); % must be set after obob_init_ft
addpath('/mnt/obob/staff/lreisinger/tinnitus/clusterjobs');

% create struct for cluster job
cfg = [];
cfg.mem = '16G';
cfg.request_time = 3000;
condor_struct = obob_slurm_create(cfg);

% data paths
datadir = '/mnt/obob/staff/lreisinger/tinnitus/data/markov_longepochs/';
outdir = '/mnt/obob/staff/lreisinger/tinnitus/data/decoding_weights';


if ~exist(outdir,'dir')
    mkdir(outdir)
end

% study_acronym = 'fs_sbg_hearing';
% all_subjects = functions.js_subjectsfromsinuhe(study_acronym);

cond = readtable('/mnt/obob/staff/lreisinger/tinnitus/others/tinnitus.csv');
all_subjects = cond.subject_id';

%% send to cluster
condor_struct = obob_slurm_addjob_cell(condor_struct, 'c_decoding', ...
  all_subjects, datadir, outdir);
obob_slurm_submit(condor_struct);