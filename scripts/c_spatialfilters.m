function c_spatialfilters(subject, datadir, outdir)

% ADD PATHS FOR FIELTRIPTOOLBOX & MVPA & ownfuns:
addpath('/mnt/obob/obob_ownft');
obob_init_ft;
cond = readtable('/mnt/obob/staff/lreisinger/tinnitus/others/tinnitus.csv');
cd('/mnt/obob/staff/lreisinger/tinnitus');
%% load data

cfg = [];

result = cond(strcmp(cond.subject_id, subject), 2);
if result{1,1} == 0
  condition = 'notinn';
  data_notinn = struct2cell(load(fullfile('data','markov_longepochs',[subject,'_',condition,'.mat']), 'data'));
  data_notinn = data_notinn{1,1};
  all_data = ft_appenddata(cfg, data_notinn{:});
else
  condition = 'tinn';
  data_tinn = struct2cell(load(fullfile('data','markov_longepochs',[subject,'_',condition,'.mat']), 'data'));
  data_tinn = data_tinn{1,1};
  all_data = ft_appenddata(cfg, data_tinn{:});
end
%% get files

j = 0;
directory = {};

if ismember(subject,{'19521226mlsa', '19550919eipc', '19560511vrkn', '19570515aohb', '19571121eibg', '19591018mrrt', '19630827tehn', '19690703puse', '19720324kash', '19910612crke'}) %use trans_anverage for these subjects
  
  while isempty(directory)
    j = j + 1;
    files = dir(fullfile(datadir(j).folder,datadir(j).name,  [subject,'_markov_blk*_trans_average_sss.fif']));
    directory = files;
  end 
  
else
  while isempty(directory)
    j = j + 1;
    files = dir(fullfile(datadir(j).folder,datadir(j).name,  [subject,'_markov_blk*_trans_sss.fif']));
    directory = files;
  end
end

%% blocks
for i_blocks = 1: length(directory)
  
  
  filename = fullfile(directory(i_blocks).folder, directory(i_blocks).name);
  
  
  cfg = [];
  cfg.trialdef.triallength = inf;
  cfg.dataset = filename;
  cfg = ft_definetrial(cfg);
  
  data_raw{i_blocks} = ft_preprocessing(cfg);
  
end
%% append data
cfg = [];
cfg.appenddim = 'rpt';
data_leadfield = ft_appenddata(cfg, data_raw{:});
% data_leadfield.grad = data_raw.grad;
%% do ica (if possible)
if isfile(fullfile('data','ICA','ICA_RR','ica_',[subject,'_hdr.mat']))
  load(fullfile('data','ICA','ICA_RR','ica_',[subject,'_hdr.mat']),'ica_components');
  load(fullfile('data','ICA','ICA_RR','reject_components_',[subject,'_hdr.mat']),'reject_components');
  cfg = [];
  cfg.component = reject_components;
  data_leadfield= ft_rejectcomponent(cfg, ica_components, data_leadfield);
end
%% load mri models etc.
load(fullfile('data','headmodels',[subject,'.mat']),'mri_aligned');
load(fullfile('data','headmodels',[subject,'.mat']),'hdm');
load('/mnt/obob/staff/lreisinger/tinnitus/mni_grid_1_cm_2982pnts.mat');

template_grid = ft_convert_units(template_grid,'m');
%% create cov matrix

cfg = [];
cfg.channel = 'MEG';
all_data_select = ft_selectdata(cfg, all_data);

%% Do Covariance matrix on equalized data and remove Visual

cfg = [];
cfg.covariance='yes';
cfg.covariancewindow = 'all';
cfg.keeptrials = 'no';

data_filter = ft_timelockanalysis(cfg, all_data_select);

%% always look after grad definition
data_filter.grad = all_data_select.grad;
cfg=[];
cfg.mri=mri_aligned;
cfg.grid.warpmni='yes';
cfg.grid.template = template_grid;
cfg.grid.nonlinear='no';
cfg.grid.unit='m';
individual_grid = ft_prepare_sourcemodel(cfg);
%% calc leadfield
cfg = [];
cfg.grid = individual_grid;
cfg.vol = hdm; % subject specific headmodel /volume
cfg.normalize = 'yes';
subject_leadfield = ft_prepare_leadfield(cfg, data_leadfield);
%% calc spatial_filter
cfg = [];
cfg.grid = subject_leadfield;
cfg.fixedori = 'yes';
cfg.regfac = '5%';
spatial_filter = obob_svs_compute_spat_filters(cfg, data_filter);

save(fullfile(outdir, [subject, '_spatial_filter.mat']),'individual_grid', 'subject_leadfield','spatial_filter','-v7.3');

end