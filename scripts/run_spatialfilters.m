%% RUNNING spatial filters ON CLUSTER (all_subjects):
% add paths & init packages:
clc
clear all
addpath('/mnt/obob/obob_ownft');

cfg = [];
cfg.package.plus_slurm = true;
obob_init_ft(cfg); % Initialize obob_ownft

addpath('/mnt/obob/staff/jschubert/myfuns'); % must be set after obob_init_ft
addpath('/mnt/obob/staff/lreisinger/tinnitus/clusterjobs');

% create struct for cluster job
cfg = [];
cfg.mem = '16G';
cfg.request_time = 3000;
condor_struct = obob_slurm_create(cfg);

cd('/mnt/obob/staff/lreisinger/tinnitus');

% data paths
raw_datadir = dir('/mnt/sinuhe/data_raw/fs_sbg_hearing/subject_subject/');
outdir = '/mnt/obob/staff/lreisinger/tinnitus/data/headmodels/';
cond = readtable('/mnt/obob/staff/lreisinger/tinnitus/others/tinnitus.csv');

block = 'markov';

if ~exist(outdir,'dir')
    mkdir(outdir)
end

% get subjects

% cond = readtable('/mnt/obob/staff/lreisinger/tinnitus/others/tinnitus.csv');
% sub = cond(:, 1);
% subcell = table2cell(rows2vars(sub));
% all_subjects = subcell(1,[2:81]);

study_acronym = 'fs_sbg_hearing';
all_subjects = functions.js_subjectsfromsinuhe(study_acronym);
bad_subjects = {'19800616mrgu', '19970203urmr', '19640129mrlh', '19531024lirs', '19630516mgsh', '19510906sewg'};
all_subjects = setdiff(all_subjects, bad_subjects);

%% send to cluster
condor_struct = obob_slurm_addjob_cell(condor_struct, 'c_spatialfilters', ...
  all_subjects, raw_datadir, outdir);
obob_slurm_submit(condor_struct);