function c_markov_preproc(subject_id, study_acronym, ica_dir, outdir, cfg_ppp)

%% ADD PATHS FOR FIELTRIPTOOLBOX & MVPA & ownfuns:

addpath('/mnt/obob/obob_ownft');
obob_init_ft; % Initialize obob_ownft

addpath(genpath('/mnt/obob/staff/jschubert/toolboxes/MVPA-Light'));
startup_MVPA_Light; % Initialize MVPA_Light

addpath('/mnt/obob/staff/jschubert/myfuns');
addpath('/mnt/obob/staff/lreisinger/tinnitus/clusterjobs');

cond = readtable('/mnt/obob/staff/lreisinger/tinnitus/others/tinnitus.csv');
            
%%
%%%%%%%%%%%%%%%%%%%%%%%% DATA PREPROCESSING %%%%%%%%%%%%%%%%%%%%%%%%%%%%
for blocknr = 1:2
  block = ['markov_b*k',num2str(blocknr)];
  
  all_files = js_fnamefromsinuhe(study_acronym, subject_id, block); % get filedir/filename
  
  %% loop through all splitted block-files:
  if iscell(all_files)
    all_splits = cell(length(all_files),1);
  else
    all_splits = 1;
  end
  
  for i = 1:length(all_splits)
    
    %% Filter data before ICA (use continuous data)
    % load data
    if iscell(all_files)
      fname = all_files{i};
    else
      fname = all_files;
    end
    
    cfg = [];
    cfg.dataset = fname;
    cfg.trialdef.ntrials = 1;
    cfg.trialdef.triallength = Inf;
    cfg = ft_definetrial(cfg);
    
    cfg.channel = cfg_ppp.channels;
    % highpass
    cfg.hpfilter = 'yes';
    cfg.hpfreq = cfg_ppp.hp; % 1Hz Hp-Frequency
    cfg.hpinstabilityfix = 'split';
    %cfg.hpfilttype = 'firws';
    %cfg.hpfiltwintype = 'kaiser';
    %cfg.hpfiltdir = 'onepass-zerophase';
    % low-pass
    cfg.lpfilter = 'yes';
    cfg.lpfreq = cfg_ppp.lp; % 100Hz Lp-Frequency
    %cfg.lpfilttype = 'firws';
    %cfg.lpfiltwintype = 'kaiser';
    %cfg.lpfiltdir = 'onepass-zerophase';
    tmp_data = ft_preprocessing(cfg);
    
    % downsample to 1000Hz for speed & memory
    cfg = [];
    cfg.resamplefs = 1000;
    cfg.detrend = 'no';
    cfg.demean = 'no';
    cfg.feedback = 'no';
    cfg.method = 'downsample';
    tmp_data = ft_resampledata(cfg,tmp_data);
    if iscell(all_splits)
      all_splits{i} = tmp_data;
    end
  end % of loop through all split-files
  
  % append all splits
  if iscell(all_splits)
    cfg = [];
    cfg.keepsampleinfo = 'no'; %!!!
    tmp_data = ft_appenddata(cfg, all_splits{:});
  end
  %% ica cleaning
  % get fnames
  fname_ica = fullfile(fullfile(ica_dir,['ica_', subject_id, '_hdr' ,'.mat']));
  fname_rejectcomp = fullfile(ica_dir,['reject_components_', subject_id, '_hdr' ,'.mat']);
  
  load(fname_ica,'ica_components');
  load(fname_rejectcomp,'reject_components');
  
  %% reject components:
  cfg = [];
  cfg.component = reject_components;
  tmp_data = ft_rejectcomponent(cfg, ica_components, tmp_data);
  
  
  % add trialinfo
  cfg = [];
  cfg.channel = cfg_ppp.channels;
  cfg.dataset = all_files;
  cfg.trialdef.prestim = cfg_ppp.epoch.presecs;
  cfg.trialdef.poststim = cfg_ppp.epoch.postsecs;
  cfg.trialfun = 'functions.trialfun_markov';
  trials = ft_definetrial(cfg);
  % downsample defined trials!!! --> if 10k
  if iscell(all_files)
    trials.trl(:,1:3) = round(trials.trl(:,1:3) / 10);
  end
  % cut into epochs
  tmp_data = ft_redefinetrial(trials, tmp_data);
  
  %trials.trl = trials.trl(3:end,:)
  
  % fix the stimulus delay
  cfg = [];
  cfg.offset = -16; % 16 at 1000Hz
  tmp_data = ft_redefinetrial(cfg,tmp_data);
  
  % downsampling
  cfg = [];
  cfg.method = 'downsample';
  cfg.resamplefs = cfg_ppp.res;
  tmp_data = ft_resampledata(cfg,tmp_data);
  
  data{blocknr} = tmp_data;
  
end % of loop through all  blocks
            
%% get condition and save

result = cond(strcmp(cond.subject_id, subject_id), 2);

if result{1,1} == 0
  save(fullfile(outdir, [subject_id, '_notinn.mat']), 'data');
else
  save(fullfile(outdir, [subject_id, '_tinn.mat']), 'data');
end
             
            
end % of run fun

        

