%%% plot weights of the decoder trained on RdSND
clear all; close all;

addpath('/mnt/obob/obob_ownft');
obob_init_ft; % Initialize obob_ownft

addpath(genpath('/mnt/obob/staff/jschubert/toolboxes/MVPA-Light'));
startup_MVPA_Light; % Initialize MVPA_Light

addpath('/mnt/obob/staff/jschubert/myfuns');
addpath('/mnt/obob/staff/jschubert/helpers');

addpath /mnt/obob/staff/gdemarchi/mattools/;
addpath /mnt/obob/staff/gdemarchi/git/export_fig/;
addpath /mnt/obob/staff/gdemarchi/DataAnalysis/tinnitusMarkov/decoding/functions/

datadir = '/mnt/obob/staff/lreisinger/tinnitus/data/headmodels/';

cond = readtable('/mnt/obob/staff/lreisinger/tinnitus/others/tinnitus.csv');
sub = cond(:, 1);
subcell = table2cell(rows2vars(sub));
all_subjects = subcell(1,[2:81]);  
% bad_subjects = {'19930727agwl', '19720906ainz', '19670511anel', '19640509kthl', '19630713mrsh', '19590330hnkw', '19581220wnyu','19570515aohb','19521226mlsa','19530712mrri','19520723angc','19480905mtbu','19890621rssh'};
% all_subjects = setdiff(all_subjects, bad_subjects);
%all_subjects = subcell(1,[3:15]);

%% load the weights-projected-onto-source virtual sensons
clear data_* tmpsrc source*

for subi = 1%length(all_subjects)
  subject_id = all_subjects{subi};
%   cfg = [];
%   result = cond(strcmp(cond.subject_id, subject_id), 2);
%   if result{1,1} == 1
%    condition = 'tinn'; 
    load(fullfile(datadir,[subject_id,'_beam.mat']),'data_source')
    data = data_source;
    
%    condition = 'notinn'; 
%      load(fullfile(datadir,[subject_id,'_beam.mat']),'data_source')
%      data_notinn{subi} = data_source;
%      
  %end 
end

%data_tinn = data_tinn(~cellfun(@isempty, data_tinn));
%data = data(~cellfun(@isempty, data))


%% single subject baseline correction
for iFile=1:length(data)
  data_abs=data;
  data_abs.avg=abs(data.avg);
end


for iFile=1:length(data)
  
  cfg=[];
  cfg.baseline=  [-0.05 0];  %
  %cfg.baselinetype='relchange';
  cfg.baselinetype='absolute';
  data_avg=obob_svs_timelockbaseline(cfg,  data_abs);
  
end


%ga_tinn_group = ft_timelockgrandaverage([],data_tinn_avg{:});

% 
% for iFile=1:length(data_notinn)
%   cfg=[];
%   cfg.baseline=  [-0.05 0];  %
%   cfg.baselinetype='relchange';
%   data_notinn_avg{iFile}=obob_svs_timelockbaseline(cfg,  data_notinn{iFile});
% end
% 
% 
% cfg = [];
% cfg.keepindividual = 'yes' ;
% tmpavg = ft_timelockgrandaverage(cfg,data_abs{:});
% 
% tmpavg.avg = squeeze(mean(tmpavg.individual));
% tmpavg.avg2 = squeeze(median(tmpavg.individual));
% 
% plot(mean(tmpavg.avg))
% plot(mean(tmpavg.avg2))
% plot(squeeze(mean(tmpavg.individual(13,:,:),2)))  

%% tmpW plot

for subi=1%:length(all_subjects)
  
    subject_id = all_subjects{subi};
    load(fullfile(datadir,[subject_id,'_beam.mat']),'tmpW');
    gfp = ft_globalmeanfield([],tmpW);
    gf{subi} = gfp;
    
end
 

for subi=1:length(all_subjects)
 
  gfAll(subi,:)= gf{subi}.avg;    
end

timeAll = gf{1}.time;
plot(timeAll,gfAll');

%% schleife

avg = 0;
for subi=1:length(all_subjects)
    subject_id = all_subjects{subi};
    load(fullfile(datadir,[subject_id,'_beam.mat']),'data_source')
    data_source.avg=abs(data_source.avg);
    
%     cfg=[];
%     cfg.baseline=  [-0.05 0];  %
%     cfg.baselinetype='relchange';
%     data_source=obob_svs_timelockbaseline(cfg,  data_source);
    tmp = data_source;
    
    avg = avg + tmp.avg;
    clear tmp data_source
    subi
  
end

avg_avg = avg / length(all_subjects);

avg_all = ga_notinn_all;
avg_all.avg = avg_avg;

% abs and baseline
  data_abs=data;
  data_abs.avg=abs(avg_all.avg);

  
  cfg=[];
  cfg.baseline=  [-0.05 0];  %
  %cfg.baselinetype='relchange';
  cfg.baselinetype='absolute';
  data_avg=obob_svs_timelockbaseline(cfg,  data_abs);
  

%% load groups

load(fullfile(datadir,'source_ga_groups','ga_tinn_all.mat'));
load(fullfile(datadir,'source_ga_groups','ga_notinn_all.mat'));

%% source plotting part
%load mni_grid_1_5_cm_889pnts.mat
load mni_grid_1_cm_2982pnts.mat
load standard_mri_better.mat
load standard_mri_better_segmented.mat

%% virtual sensors to source structure ...
% early component a.k.a. W1
cfg=[];
cfg.sourcegrid =  template_grid;
cfg.parameter={'avg'};
cfg.toilim=[.1 .2];
cfg.mri = mri; % mri/mri_better depends ...
source2plotE = obob_svs_virtualsens2source(cfg, data_avg);   %13,27
%source2plotE = obob_svs_virtualsens2source(cfg, avg_all);

%late componenta.k.a. W2
cfg.toilim=[0.45 0.55];
source2plotL = obob_svs_virtualsens2source(cfg, data_avg);


%% finally source plots
% ortho
cfg = [];
cfg.funparameter = 'avg';
cfg.maskparameter =cfg.funparameter;
cfg.funcolormap = 'jet';
ft_sourceplot(cfg, source2plotL);

%% surface plot(s)
cfg = [];
cfg.method         = 'surface';
cfg.funparameter   = 'avg';
cfg.funcolorlim    = 'zeromax';
cfg.funcolormap    = 'hot';
cfg.projmethod     = 'nearest';
%cfg.projthresh     = 0.66;
cfg.camlight       = 'no';
cfg.colorbar = 'yes';
cfg.maskparameter= cfg.funparameter;%
cfg.surfinflated   = 'surface_inflated_both_caret.mat';
%cfg.funcolorlim   = [-3 3]; %keep it consistent

% early component
ft_sourceplot(cfg, source2plotE);
% view([90, 0])
% colormap(flipud(hot))
% material dull
% camlight
% save left/right views sepatare
% saveas(gcf,[fileDir 'EarlyRight_th50.tif']);
view([-90, 0])
delete(findall(gcf,'Type','light')); % remove the old lights
colormap(flipud(hot))
material dull
camlight
% saveas(gcf,[fileDir 'EarlyLeft_th50.tif']);
%%
% late component
ft_sourceplot(cfg, source2plotL);
view([90, 0])
colormap(flipud(hot))
material dull
camlight
% save left/right views sepatare
% saveas(gcf,[fileDir 'LateRight_th50.tif']);
view([-90, 0])
delete(findall(gcf,'Type','light')); % remove the old lights
colormap(flipud(hot))
material dull
camlight
% saveas(gcf,[fileDir 'LateLeft_th50.tif']);

